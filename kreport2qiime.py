from Bio import Entrez
from ete3 import NCBITaxa
import argparse
from os.path import isfile
from os import unlink
import pandas as pd

parser = argparse.ArgumentParser(description='Translate kraken report to qiime2 importable taxonomy table')
parser.add_argument('--in-kreports', type=str, required=True, nargs='+',
                    help='kraken report(s)')
parser.add_argument('--out-class', type=str, required=True, help='qiime2 classification')
parser.add_argument('--no-header', action='store_true',
                    help='Return table without header')
args = parser.parse_args()

ncbi = NCBITaxa()
Entrez.email = "carlos.delannoy@wur.nl"

saved_ranks = {'superkingdom': 'su__', 'kingdom': 'k__', 'phylum': 'p__', 'class': 'c__',
               'order': 'o__', 'family': 'f__', 'genus': 'g__', 'species': 's__'}
skip_entry = False
print_threshold = 20
print_idx = 0

if isfile(args.out_class): unlink(args.out_class)

class_list = []
for ic in args.in_kreports:
    class_list.append(pd.read_csv(ic, sep='\t', header=None, names=['pct_reads', 'nb_reads', 'nb_reads_clade', 'rank', 'taxID', 'tree_txt']))
class_df = pd.concat(class_list)
class_df.drop_duplicates(subset='taxID', keep='first', inplace=True)

def get_qiime_classification(tup):
    if tup.taxID == 0:
        return "; ".join(saved_ranks.values())
    lineage_ids = ncbi.get_taxid_translator(ncbi.get_lineage(tup.taxID))
    lineage_ranks = ncbi.get_rank(lineage_ids)
    lineage_names = ncbi.get_taxid_translator(lineage_ids)
    lineage_dict = saved_ranks.copy()
    for lid in lineage_ranks:
        cur_lr = lineage_ranks[lid]
        if lineage_dict.get(cur_lr, False):
            lineage_dict[lineage_ranks[lid]] += lineage_names[lid].split()[-1]
    return "; ".join(lineage_dict.values())

# for it, tup in class_df.iterrows():
#     ftst = get_qiime_classification(tup)

class_df.loc[:, 'qiime_class'] = class_df.apply(get_qiime_classification, axis=1)


out_df = pd.DataFrame(data={'Feature ID': class_df.loc[:, 'taxID'],
                            'Taxon': class_df.qiime_class,
                            'Confidence': 1})
out_df.to_csv(args.out_class, sep='\t', header=[True, False][args.no_header], index=False)
