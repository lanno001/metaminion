[
    {
        "aggregation": "segment",
        "analysis_id": "4e7ec2b8-beaf-49ab-9371-f6fd816dc7fc",
        "basecall_1d": {
            "exit_status_dist": {
                "fail:qscore_filter": 44,
                "pass": 881
            },
            "qscore_dist_temp": [
                {
                    "count": 2,
                    "mean_qscore": 2.0
                },
                {
                    "count": 4,
                    "mean_qscore": 2.5
                },
                {
                    "count": 1,
                    "mean_qscore": 3.0
                },
                {
                    "count": 4,
                    "mean_qscore": 3.5
                },
                {
                    "count": 2,
                    "mean_qscore": 4.0
                },
                {
                    "count": 2,
                    "mean_qscore": 4.5
                },
                {
                    "count": 4,
                    "mean_qscore": 5.0
                },
                {
                    "count": 5,
                    "mean_qscore": 5.5
                },
                {
                    "count": 7,
                    "mean_qscore": 6.0
                },
                {
                    "count": 13,
                    "mean_qscore": 6.5
                },
                {
                    "count": 16,
                    "mean_qscore": 7.0
                },
                {
                    "count": 30,
                    "mean_qscore": 7.5
                },
                {
                    "count": 21,
                    "mean_qscore": 8.0
                },
                {
                    "count": 44,
                    "mean_qscore": 8.5
                },
                {
                    "count": 59,
                    "mean_qscore": 9.0
                },
                {
                    "count": 123,
                    "mean_qscore": 9.5
                },
                {
                    "count": 156,
                    "mean_qscore": 10.0
                },
                {
                    "count": 197,
                    "mean_qscore": 10.5
                },
                {
                    "count": 148,
                    "mean_qscore": 11.0
                },
                {
                    "count": 66,
                    "mean_qscore": 11.5
                },
                {
                    "count": 14,
                    "mean_qscore": 12.0
                },
                {
                    "count": 6,
                    "mean_qscore": 12.5
                },
                {
                    "count": 1,
                    "mean_qscore": 13.0
                }
            ],
            "qscore_sum_temp": {
                "count": 925,
                "mean": 10.0544900894165,
                "sum": 9300.4033203125
            },
            "read_len_events_sum_temp": 3542389,
            "seq_len_bases_dist_temp": [
                {
                    "count": 81,
                    "length": 0.0
                },
                {
                    "count": 841,
                    "length": 1000.0
                },
                {
                    "count": 3,
                    "length": 3000.0
                }
            ],
            "seq_len_bases_sum_temp": 1358576,
            "seq_len_events_dist_temp": [
                {
                    "count": 6,
                    "length": 0.0
                },
                {
                    "count": 49,
                    "length": 1000.0
                },
                {
                    "count": 44,
                    "length": 2000.0
                },
                {
                    "count": 407,
                    "length": 3000.0
                },
                {
                    "count": 392,
                    "length": 4000.0
                },
                {
                    "count": 15,
                    "length": 5000.0
                },
                {
                    "count": 5,
                    "length": 6000.0
                },
                {
                    "count": 5,
                    "length": 7000.0
                },
                {
                    "count": 1,
                    "length": 9000.0
                },
                {
                    "count": 1,
                    "length": 10000.0
                }
            ],
            "speed_bases_per_second_dist_temp": [
                {
                    "count": 1,
                    "speed": 15.0
                },
                {
                    "count": 1,
                    "speed": 53.0
                },
                {
                    "count": 1,
                    "speed": 97.0
                },
                {
                    "count": 1,
                    "speed": 103.0
                },
                {
                    "count": 1,
                    "speed": 105.0
                },
                {
                    "count": 1,
                    "speed": 111.0
                },
                {
                    "count": 1,
                    "speed": 141.0
                },
                {
                    "count": 1,
                    "speed": 177.0
                },
                {
                    "count": 1,
                    "speed": 187.0
                },
                {
                    "count": 2,
                    "speed": 191.0
                },
                {
                    "count": 1,
                    "speed": 193.0
                },
                {
                    "count": 1,
                    "speed": 207.0
                },
                {
                    "count": 2,
                    "speed": 209.0
                },
                {
                    "count": 1,
                    "speed": 217.0
                },
                {
                    "count": 1,
                    "speed": 245.0
                },
                {
                    "count": 1,
                    "speed": 255.0
                },
                {
                    "count": 1,
                    "speed": 259.0
                },
                {
                    "count": 1,
                    "speed": 265.0
                },
                {
                    "count": 1,
                    "speed": 267.0
                },
                {
                    "count": 1,
                    "speed": 269.0
                },
                {
                    "count": 2,
                    "speed": 277.0
                },
                {
                    "count": 1,
                    "speed": 283.0
                },
                {
                    "count": 2,
                    "speed": 291.0
                },
                {
                    "count": 1,
                    "speed": 299.0
                },
                {
                    "count": 2,
                    "speed": 309.0
                },
                {
                    "count": 1,
                    "speed": 311.0
                },
                {
                    "count": 1,
                    "speed": 313.0
                },
                {
                    "count": 1,
                    "speed": 315.0
                },
                {
                    "count": 2,
                    "speed": 319.0
                },
                {
                    "count": 2,
                    "speed": 321.0
                },
                {
                    "count": 1,
                    "speed": 323.0
                },
                {
                    "count": 1,
                    "speed": 325.0
                },
                {
                    "count": 4,
                    "speed": 327.0
                },
                {
                    "count": 5,
                    "speed": 331.0
                },
                {
                    "count": 1,
                    "speed": 333.0
                },
                {
                    "count": 3,
                    "speed": 335.0
                },
                {
                    "count": 1,
                    "speed": 337.0
                },
                {
                    "count": 7,
                    "speed": 339.0
                },
                {
                    "count": 3,
                    "speed": 341.0
                },
                {
                    "count": 4,
                    "speed": 343.0
                },
                {
                    "count": 7,
                    "speed": 345.0
                },
                {
                    "count": 5,
                    "speed": 347.0
                },
                {
                    "count": 8,
                    "speed": 349.0
                },
                {
                    "count": 4,
                    "speed": 351.0
                },
                {
                    "count": 6,
                    "speed": 353.0
                },
                {
                    "count": 17,
                    "speed": 355.0
                },
                {
                    "count": 10,
                    "speed": 357.0
                },
                {
                    "count": 13,
                    "speed": 359.0
                },
                {
                    "count": 15,
                    "speed": 361.0
                },
                {
                    "count": 15,
                    "speed": 363.0
                },
                {
                    "count": 20,
                    "speed": 365.0
                },
                {
                    "count": 21,
                    "speed": 367.0
                },
                {
                    "count": 23,
                    "speed": 369.0
                },
                {
                    "count": 20,
                    "speed": 371.0
                },
                {
                    "count": 28,
                    "speed": 373.0
                },
                {
                    "count": 33,
                    "speed": 375.0
                },
                {
                    "count": 18,
                    "speed": 377.0
                },
                {
                    "count": 14,
                    "speed": 379.0
                },
                {
                    "count": 22,
                    "speed": 381.0
                },
                {
                    "count": 21,
                    "speed": 383.0
                },
                {
                    "count": 23,
                    "speed": 385.0
                },
                {
                    "count": 29,
                    "speed": 387.0
                },
                {
                    "count": 33,
                    "speed": 389.0
                },
                {
                    "count": 26,
                    "speed": 391.0
                },
                {
                    "count": 24,
                    "speed": 393.0
                },
                {
                    "count": 20,
                    "speed": 395.0
                },
                {
                    "count": 32,
                    "speed": 397.0
                },
                {
                    "count": 18,
                    "speed": 399.0
                },
                {
                    "count": 24,
                    "speed": 401.0
                },
                {
                    "count": 26,
                    "speed": 403.0
                },
                {
                    "count": 23,
                    "speed": 405.0
                },
                {
                    "count": 20,
                    "speed": 407.0
                },
                {
                    "count": 29,
                    "speed": 409.0
                },
                {
                    "count": 21,
                    "speed": 411.0
                },
                {
                    "count": 13,
                    "speed": 413.0
                },
                {
                    "count": 16,
                    "speed": 415.0
                },
                {
                    "count": 10,
                    "speed": 417.0
                },
                {
                    "count": 17,
                    "speed": 419.0
                },
                {
                    "count": 11,
                    "speed": 421.0
                },
                {
                    "count": 10,
                    "speed": 423.0
                },
                {
                    "count": 11,
                    "speed": 425.0
                },
                {
                    "count": 11,
                    "speed": 427.0
                },
                {
                    "count": 8,
                    "speed": 429.0
                },
                {
                    "count": 9,
                    "speed": 431.0
                },
                {
                    "count": 2,
                    "speed": 433.0
                },
                {
                    "count": 9,
                    "speed": 435.0
                },
                {
                    "count": 3,
                    "speed": 437.0
                },
                {
                    "count": 6,
                    "speed": 439.0
                },
                {
                    "count": 8,
                    "speed": 441.0
                },
                {
                    "count": 4,
                    "speed": 443.0
                },
                {
                    "count": 7,
                    "speed": 445.0
                },
                {
                    "count": 4,
                    "speed": 447.0
                },
                {
                    "count": 5,
                    "speed": 449.0
                },
                {
                    "count": 1,
                    "speed": 451.0
                },
                {
                    "count": 6,
                    "speed": 453.0
                },
                {
                    "count": 1,
                    "speed": 455.0
                },
                {
                    "count": 3,
                    "speed": 459.0
                },
                {
                    "count": 1,
                    "speed": 461.0
                },
                {
                    "count": 3,
                    "speed": 463.0
                },
                {
                    "count": 2,
                    "speed": 465.0
                },
                {
                    "count": 2,
                    "speed": 471.0
                },
                {
                    "count": 1,
                    "speed": 473.0
                },
                {
                    "count": 1,
                    "speed": 479.0
                },
                {
                    "count": 1,
                    "speed": 487.0
                },
                {
                    "count": 1,
                    "speed": 493.0
                },
                {
                    "count": 1,
                    "speed": 499.0
                },
                {
                    "count": 1,
                    "speed": 501.0
                },
                {
                    "count": 1,
                    "speed": 507.0
                }
            ],
            "strand_median_pa": {
                "count": 925,
                "mean": 81.1472091674805,
                "sum": 75061.171875
            },
            "strand_sd_pa": {
                "count": 925,
                "mean": 9.89926910400391,
                "sum": 9156.82421875
            }
        },
        "channel_count": 423,
        "context_tags": {
            "experiment_duration_set": "1440",
            "experiment_type": "genomic_dna",
            "fast5_output_fastq_in_hdf": "1",
            "fast5_raw": "1",
            "fast5_reads_per_folder": "4000",
            "fastq_enabled": "1",
            "fastq_reads_per_file": "4000",
            "filename": "l0144169_20181211_fak22428_mn19628_sequencing_run_16srhizosphere_23805",
            "flowcell_type": "flo-min106",
            "kit_classification": "none",
            "local_basecalling": "0",
            "local_bc_comp_model": "",
            "local_bc_temp_model": "template_r9.4_450bps_5mer_raw.jsn",
            "sample_frequency": "4000",
            "sequencing_kit": "sqk-rab204",
            "user_filename_input": "16srhizosphere"
        },
        "latest_run_time": 726.727478027344,
        "levels_sums": {
            "count": 925,
            "mean": 202.833908081055,
            "open_pore_level_sum": 187621.359375
        },
        "opts": {
            "adapter_pt_range_scale": "5.200000",
            "builtin_scripts": "1",
            "calib_detect": "0",
            "calib_max_sequence_length": "3800",
            "calib_min_coverage": "0.600000",
            "calib_min_sequence_length": "3000",
            "calib_reference": "lambda_3.6kb.fasta",
            "chunk_size": "1000",
            "chunks_per_caller": "10000",
            "chunks_per_runner": "20",
            "cpu_threads_per_caller": "1",
            "device": "",
            "disable_events": "0",
            "disable_pings": "0",
            "dmean_threshold": "1.000000",
            "dmean_win_size": "2",
            "enable_trimming": "1",
            "fast5_out": "0",
            "flowcell": "FLO-MIN106",
            "gpu_runners_per_device": "20",
            "hp_correct": "1",
            "jump_threshold": "1.000000",
            "kernel_path": "",
            "kit": "SQK-RAB204",
            "max_search_len": "1000",
            "min_qscore": "7.000000",
            "model_file": "template_r9.4.1_450bps_fast_flipflop.jsn",
            "num_callers": "4",
            "overlap": "50",
            "override_scaling": "0",
            "ping_segment_duration": "60",
            "ping_url": "https://ping.oxfordnanoportal.com/basecall",
            "port": "",
            "print_workflows": "0",
            "pt_median_offset": "2.500000",
            "pt_minimum_read_start_index": "30",
            "pt_required_adapter_drop": "30.000000",
            "pt_scaling": "0",
            "qscore_filtering": "0",
            "qscore_offset": "-1.300000",
            "qscore_scale": "1.100000",
            "quiet": "0",
            "read_id_list": "",
            "records_per_fastq": "4000",
            "recursive": "0",
            "reverse_sequence": "0",
            "scaling_mad": "0.000000",
            "scaling_med": "0.000000",
            "stay_penalty": "1.000000",
            "temp_bias": "1.000000",
            "temp_weight": "1.000000",
            "trace_categories_logs": "",
            "trim_min_events": "3",
            "trim_strategy": "dna",
            "trim_threshold": "2.500000",
            "u_substitution": "0",
            "verbose_logs": "0"
        },
        "read_count": 925,
        "reads_per_channel_dist": [
            {
                "channel": 1,
                "count": 1
            },
            {
                "channel": 2,
                "count": 2
            },
            {
                "channel": 4,
                "count": 1
            },
            {
                "channel": 7,
                "count": 2
            },
            {
                "channel": 8,
                "count": 4
            },
            {
                "channel": 9,
                "count": 3
            },
            {
                "channel": 10,
                "count": 2
            },
            {
                "channel": 11,
                "count": 1
            },
            {
                "channel": 12,
                "count": 8
            },
            {
                "channel": 13,
                "count": 1
            },
            {
                "channel": 14,
                "count": 1
            },
            {
                "channel": 15,
                "count": 1
            },
            {
                "channel": 16,
                "count": 1
            },
            {
                "channel": 17,
                "count": 2
            },
            {
                "channel": 18,
                "count": 2
            },
            {
                "channel": 19,
                "count": 1
            },
            {
                "channel": 20,
                "count": 1
            },
            {
                "channel": 21,
                "count": 4
            },
            {
                "channel": 24,
                "count": 3
            },
            {
                "channel": 25,
                "count": 1
            },
            {
                "channel": 26,
                "count": 4
            },
            {
                "channel": 27,
                "count": 4
            },
            {
                "channel": 29,
                "count": 2
            },
            {
                "channel": 30,
                "count": 4
            },
            {
                "channel": 31,
                "count": 3
            },
            {
                "channel": 32,
                "count": 1
            },
            {
                "channel": 33,
                "count": 6
            },
            {
                "channel": 34,
                "count": 4
            },
            {
                "channel": 35,
                "count": 1
            },
            {
                "channel": 36,
                "count": 2
            },
            {
                "channel": 37,
                "count": 2
            },
            {
                "channel": 38,
                "count": 2
            },
            {
                "channel": 39,
                "count": 1
            },
            {
                "channel": 41,
                "count": 3
            },
            {
                "channel": 42,
                "count": 2
            },
            {
                "channel": 43,
                "count": 5
            },
            {
                "channel": 44,
                "count": 5
            },
            {
                "channel": 45,
                "count": 4
            },
            {
                "channel": 47,
                "count": 2
            },
            {
                "channel": 49,
                "count": 2
            },
            {
                "channel": 50,
                "count": 4
            },
            {
                "channel": 53,
                "count": 2
            },
            {
                "channel": 55,
                "count": 1
            },
            {
                "channel": 56,
                "count": 2
            },
            {
                "channel": 58,
                "count": 1
            },
            {
                "channel": 59,
                "count": 2
            },
            {
                "channel": 60,
                "count": 2
            },
            {
                "channel": 61,
                "count": 2
            },
            {
                "channel": 62,
                "count": 1
            },
            {
                "channel": 67,
                "count": 3
            },
            {
                "channel": 68,
                "count": 2
            },
            {
                "channel": 69,
                "count": 1
            },
            {
                "channel": 70,
                "count": 2
            },
            {
                "channel": 71,
                "count": 1
            },
            {
                "channel": 72,
                "count": 5
            },
            {
                "channel": 73,
                "count": 3
            },
            {
                "channel": 74,
                "count": 2
            },
            {
                "channel": 75,
                "count": 4
            },
            {
                "channel": 77,
                "count": 5
            },
            {
                "channel": 78,
                "count": 1
            },
            {
                "channel": 79,
                "count": 2
            },
            {
                "channel": 80,
                "count": 1
            },
            {
                "channel": 81,
                "count": 1
            },
            {
                "channel": 82,
                "count": 1
            },
            {
                "channel": 83,
                "count": 1
            },
            {
                "channel": 84,
                "count": 3
            },
            {
                "channel": 85,
                "count": 1
            },
            {
                "channel": 86,
                "count": 1
            },
            {
                "channel": 87,
                "count": 3
            },
            {
                "channel": 88,
                "count": 2
            },
            {
                "channel": 90,
                "count": 2
            },
            {
                "channel": 91,
                "count": 7
            },
            {
                "channel": 92,
                "count": 3
            },
            {
                "channel": 93,
                "count": 2
            },
            {
                "channel": 95,
                "count": 1
            },
            {
                "channel": 96,
                "count": 2
            },
            {
                "channel": 97,
                "count": 3
            },
            {
                "channel": 98,
                "count": 2
            },
            {
                "channel": 99,
                "count": 1
            },
            {
                "channel": 100,
                "count": 3
            },
            {
                "channel": 101,
                "count": 2
            },
            {
                "channel": 102,
                "count": 1
            },
            {
                "channel": 103,
                "count": 5
            },
            {
                "channel": 104,
                "count": 1
            },
            {
                "channel": 105,
                "count": 1
            },
            {
                "channel": 106,
                "count": 1
            },
            {
                "channel": 107,
                "count": 3
            },
            {
                "channel": 109,
                "count": 2
            },
            {
                "channel": 110,
                "count": 2
            },
            {
                "channel": 111,
                "count": 1
            },
            {
                "channel": 112,
                "count": 1
            },
            {
                "channel": 113,
                "count": 2
            },
            {
                "channel": 114,
                "count": 4
            },
            {
                "channel": 115,
                "count": 2
            },
            {
                "channel": 117,
                "count": 1
            },
            {
                "channel": 118,
                "count": 3
            },
            {
                "channel": 119,
                "count": 1
            },
            {
                "channel": 120,
                "count": 3
            },
            {
                "channel": 121,
                "count": 1
            },
            {
                "channel": 122,
                "count": 2
            },
            {
                "channel": 123,
                "count": 1
            },
            {
                "channel": 124,
                "count": 1
            },
            {
                "channel": 125,
                "count": 2
            },
            {
                "channel": 126,
                "count": 1
            },
            {
                "channel": 127,
                "count": 2
            },
            {
                "channel": 128,
                "count": 5
            },
            {
                "channel": 129,
                "count": 7
            },
            {
                "channel": 130,
                "count": 2
            },
            {
                "channel": 131,
                "count": 2
            },
            {
                "channel": 132,
                "count": 2
            },
            {
                "channel": 134,
                "count": 3
            },
            {
                "channel": 135,
                "count": 2
            },
            {
                "channel": 136,
                "count": 2
            },
            {
                "channel": 137,
                "count": 1
            },
            {
                "channel": 138,
                "count": 2
            },
            {
                "channel": 139,
                "count": 2
            },
            {
                "channel": 140,
                "count": 2
            },
            {
                "channel": 141,
                "count": 1
            },
            {
                "channel": 143,
                "count": 3
            },
            {
                "channel": 144,
                "count": 1
            },
            {
                "channel": 145,
                "count": 4
            },
            {
                "channel": 147,
                "count": 3
            },
            {
                "channel": 148,
                "count": 1
            },
            {
                "channel": 149,
                "count": 2
            },
            {
                "channel": 150,
                "count": 2
            },
            {
                "channel": 151,
                "count": 2
            },
            {
                "channel": 152,
                "count": 4
            },
            {
                "channel": 153,
                "count": 2
            },
            {
                "channel": 155,
                "count": 3
            },
            {
                "channel": 156,
                "count": 1
            },
            {
                "channel": 157,
                "count": 4
            },
            {
                "channel": 158,
                "count": 4
            },
            {
                "channel": 159,
                "count": 2
            },
            {
                "channel": 160,
                "count": 3
            },
            {
                "channel": 161,
                "count": 1
            },
            {
                "channel": 162,
                "count": 2
            },
            {
                "channel": 163,
                "count": 1
            },
            {
                "channel": 165,
                "count": 1
            },
            {
                "channel": 166,
                "count": 2
            },
            {
                "channel": 167,
                "count": 3
            },
            {
                "channel": 168,
                "count": 4
            },
            {
                "channel": 170,
                "count": 2
            },
            {
                "channel": 171,
                "count": 2
            },
            {
                "channel": 172,
                "count": 2
            },
            {
                "channel": 174,
                "count": 2
            },
            {
                "channel": 176,
                "count": 2
            },
            {
                "channel": 177,
                "count": 1
            },
            {
                "channel": 179,
                "count": 2
            },
            {
                "channel": 181,
                "count": 1
            },
            {
                "channel": 183,
                "count": 1
            },
            {
                "channel": 184,
                "count": 1
            },
            {
                "channel": 185,
                "count": 2
            },
            {
                "channel": 186,
                "count": 1
            },
            {
                "channel": 187,
                "count": 4
            },
            {
                "channel": 188,
                "count": 1
            },
            {
                "channel": 190,
                "count": 2
            },
            {
                "channel": 191,
                "count": 2
            },
            {
                "channel": 192,
                "count": 3
            },
            {
                "channel": 193,
                "count": 1
            },
            {
                "channel": 194,
                "count": 1
            },
            {
                "channel": 195,
                "count": 1
            },
            {
                "channel": 196,
                "count": 3
            },
            {
                "channel": 197,
                "count": 3
            },
            {
                "channel": 199,
                "count": 4
            },
            {
                "channel": 200,
                "count": 1
            },
            {
                "channel": 201,
                "count": 2
            },
            {
                "channel": 202,
                "count": 4
            },
            {
                "channel": 203,
                "count": 2
            },
            {
                "channel": 204,
                "count": 2
            },
            {
                "channel": 205,
                "count": 3
            },
            {
                "channel": 206,
                "count": 3
            },
            {
                "channel": 207,
                "count": 3
            },
            {
                "channel": 208,
                "count": 2
            },
            {
                "channel": 209,
                "count": 1
            },
            {
                "channel": 210,
                "count": 1
            },
            {
                "channel": 212,
                "count": 2
            },
            {
                "channel": 213,
                "count": 3
            },
            {
                "channel": 214,
                "count": 1
            },
            {
                "channel": 215,
                "count": 1
            },
            {
                "channel": 216,
                "count": 2
            },
            {
                "channel": 217,
                "count": 1
            },
            {
                "channel": 218,
                "count": 4
            },
            {
                "channel": 220,
                "count": 2
            },
            {
                "channel": 221,
                "count": 2
            },
            {
                "channel": 222,
                "count": 1
            },
            {
                "channel": 223,
                "count": 2
            },
            {
                "channel": 227,
                "count": 1
            },
            {
                "channel": 228,
                "count": 2
            },
            {
                "channel": 231,
                "count": 1
            },
            {
                "channel": 232,
                "count": 3
            },
            {
                "channel": 233,
                "count": 1
            },
            {
                "channel": 234,
                "count": 5
            },
            {
                "channel": 235,
                "count": 3
            },
            {
                "channel": 236,
                "count": 1
            },
            {
                "channel": 237,
                "count": 2
            },
            {
                "channel": 238,
                "count": 2
            },
            {
                "channel": 240,
                "count": 3
            },
            {
                "channel": 241,
                "count": 1
            },
            {
                "channel": 242,
                "count": 3
            },
            {
                "channel": 243,
                "count": 1
            },
            {
                "channel": 245,
                "count": 3
            },
            {
                "channel": 246,
                "count": 1
            },
            {
                "channel": 248,
                "count": 2
            },
            {
                "channel": 249,
                "count": 2
            },
            {
                "channel": 250,
                "count": 1
            },
            {
                "channel": 251,
                "count": 6
            },
            {
                "channel": 252,
                "count": 1
            },
            {
                "channel": 254,
                "count": 1
            },
            {
                "channel": 255,
                "count": 1
            },
            {
                "channel": 256,
                "count": 1
            },
            {
                "channel": 257,
                "count": 2
            },
            {
                "channel": 258,
                "count": 2
            },
            {
                "channel": 259,
                "count": 1
            },
            {
                "channel": 260,
                "count": 2
            },
            {
                "channel": 261,
                "count": 2
            },
            {
                "channel": 262,
                "count": 1
            },
            {
                "channel": 263,
                "count": 7
            },
            {
                "channel": 264,
                "count": 4
            },
            {
                "channel": 265,
                "count": 1
            },
            {
                "channel": 266,
                "count": 5
            },
            {
                "channel": 267,
                "count": 1
            },
            {
                "channel": 268,
                "count": 2
            },
            {
                "channel": 269,
                "count": 2
            },
            {
                "channel": 270,
                "count": 1
            },
            {
                "channel": 271,
                "count": 2
            },
            {
                "channel": 272,
                "count": 1
            },
            {
                "channel": 274,
                "count": 2
            },
            {
                "channel": 276,
                "count": 6
            },
            {
                "channel": 277,
                "count": 1
            },
            {
                "channel": 279,
                "count": 2
            },
            {
                "channel": 280,
                "count": 4
            },
            {
                "channel": 281,
                "count": 4
            },
            {
                "channel": 282,
                "count": 3
            },
            {
                "channel": 283,
                "count": 3
            },
            {
                "channel": 285,
                "count": 2
            },
            {
                "channel": 286,
                "count": 1
            },
            {
                "channel": 287,
                "count": 1
            },
            {
                "channel": 289,
                "count": 4
            },
            {
                "channel": 290,
                "count": 1
            },
            {
                "channel": 291,
                "count": 1
            },
            {
                "channel": 292,
                "count": 5
            },
            {
                "channel": 293,
                "count": 1
            },
            {
                "channel": 295,
                "count": 3
            },
            {
                "channel": 296,
                "count": 1
            },
            {
                "channel": 297,
                "count": 3
            },
            {
                "channel": 298,
                "count": 2
            },
            {
                "channel": 299,
                "count": 2
            },
            {
                "channel": 300,
                "count": 1
            },
            {
                "channel": 301,
                "count": 1
            },
            {
                "channel": 302,
                "count": 1
            },
            {
                "channel": 303,
                "count": 1
            },
            {
                "channel": 304,
                "count": 1
            },
            {
                "channel": 305,
                "count": 1
            },
            {
                "channel": 306,
                "count": 2
            },
            {
                "channel": 307,
                "count": 4
            },
            {
                "channel": 308,
                "count": 1
            },
            {
                "channel": 309,
                "count": 1
            },
            {
                "channel": 312,
                "count": 6
            },
            {
                "channel": 314,
                "count": 2
            },
            {
                "channel": 317,
                "count": 1
            },
            {
                "channel": 318,
                "count": 2
            },
            {
                "channel": 319,
                "count": 1
            },
            {
                "channel": 320,
                "count": 3
            },
            {
                "channel": 321,
                "count": 1
            },
            {
                "channel": 322,
                "count": 4
            },
            {
                "channel": 323,
                "count": 5
            },
            {
                "channel": 324,
                "count": 3
            },
            {
                "channel": 325,
                "count": 3
            },
            {
                "channel": 326,
                "count": 3
            },
            {
                "channel": 327,
                "count": 1
            },
            {
                "channel": 328,
                "count": 4
            },
            {
                "channel": 329,
                "count": 6
            },
            {
                "channel": 330,
                "count": 2
            },
            {
                "channel": 331,
                "count": 1
            },
            {
                "channel": 333,
                "count": 2
            },
            {
                "channel": 334,
                "count": 1
            },
            {
                "channel": 336,
                "count": 5
            },
            {
                "channel": 337,
                "count": 1
            },
            {
                "channel": 339,
                "count": 2
            },
            {
                "channel": 340,
                "count": 1
            },
            {
                "channel": 342,
                "count": 4
            },
            {
                "channel": 343,
                "count": 2
            },
            {
                "channel": 344,
                "count": 3
            },
            {
                "channel": 345,
                "count": 6
            },
            {
                "channel": 346,
                "count": 2
            },
            {
                "channel": 347,
                "count": 1
            },
            {
                "channel": 348,
                "count": 2
            },
            {
                "channel": 349,
                "count": 2
            },
            {
                "channel": 350,
                "count": 4
            },
            {
                "channel": 351,
                "count": 1
            },
            {
                "channel": 352,
                "count": 1
            },
            {
                "channel": 353,
                "count": 2
            },
            {
                "channel": 354,
                "count": 3
            },
            {
                "channel": 355,
                "count": 1
            },
            {
                "channel": 356,
                "count": 4
            },
            {
                "channel": 358,
                "count": 3
            },
            {
                "channel": 359,
                "count": 1
            },
            {
                "channel": 361,
                "count": 3
            },
            {
                "channel": 362,
                "count": 3
            },
            {
                "channel": 363,
                "count": 3
            },
            {
                "channel": 365,
                "count": 1
            },
            {
                "channel": 366,
                "count": 1
            },
            {
                "channel": 367,
                "count": 3
            },
            {
                "channel": 368,
                "count": 2
            },
            {
                "channel": 369,
                "count": 2
            },
            {
                "channel": 370,
                "count": 2
            },
            {
                "channel": 371,
                "count": 2
            },
            {
                "channel": 372,
                "count": 2
            },
            {
                "channel": 373,
                "count": 3
            },
            {
                "channel": 374,
                "count": 3
            },
            {
                "channel": 375,
                "count": 1
            },
            {
                "channel": 376,
                "count": 2
            },
            {
                "channel": 377,
                "count": 1
            },
            {
                "channel": 378,
                "count": 2
            },
            {
                "channel": 379,
                "count": 1
            },
            {
                "channel": 380,
                "count": 4
            },
            {
                "channel": 381,
                "count": 1
            },
            {
                "channel": 382,
                "count": 2
            },
            {
                "channel": 384,
                "count": 5
            },
            {
                "channel": 385,
                "count": 2
            },
            {
                "channel": 386,
                "count": 1
            },
            {
                "channel": 387,
                "count": 2
            },
            {
                "channel": 388,
                "count": 1
            },
            {
                "channel": 389,
                "count": 2
            },
            {
                "channel": 390,
                "count": 2
            },
            {
                "channel": 393,
                "count": 3
            },
            {
                "channel": 394,
                "count": 2
            },
            {
                "channel": 395,
                "count": 2
            },
            {
                "channel": 396,
                "count": 1
            },
            {
                "channel": 397,
                "count": 3
            },
            {
                "channel": 398,
                "count": 1
            },
            {
                "channel": 399,
                "count": 1
            },
            {
                "channel": 400,
                "count": 2
            },
            {
                "channel": 401,
                "count": 4
            },
            {
                "channel": 402,
                "count": 4
            },
            {
                "channel": 403,
                "count": 4
            },
            {
                "channel": 405,
                "count": 2
            },
            {
                "channel": 406,
                "count": 4
            },
            {
                "channel": 407,
                "count": 2
            },
            {
                "channel": 408,
                "count": 1
            },
            {
                "channel": 409,
                "count": 1
            },
            {
                "channel": 410,
                "count": 2
            },
            {
                "channel": 411,
                "count": 3
            },
            {
                "channel": 412,
                "count": 1
            },
            {
                "channel": 413,
                "count": 2
            },
            {
                "channel": 415,
                "count": 1
            },
            {
                "channel": 417,
                "count": 1
            },
            {
                "channel": 419,
                "count": 2
            },
            {
                "channel": 420,
                "count": 4
            },
            {
                "channel": 422,
                "count": 2
            },
            {
                "channel": 423,
                "count": 1
            },
            {
                "channel": 424,
                "count": 2
            },
            {
                "channel": 425,
                "count": 1
            },
            {
                "channel": 426,
                "count": 2
            },
            {
                "channel": 427,
                "count": 1
            },
            {
                "channel": 428,
                "count": 1
            },
            {
                "channel": 429,
                "count": 1
            },
            {
                "channel": 430,
                "count": 1
            },
            {
                "channel": 431,
                "count": 2
            },
            {
                "channel": 432,
                "count": 3
            },
            {
                "channel": 433,
                "count": 1
            },
            {
                "channel": 434,
                "count": 2
            },
            {
                "channel": 435,
                "count": 2
            },
            {
                "channel": 437,
                "count": 2
            },
            {
                "channel": 438,
                "count": 2
            },
            {
                "channel": 440,
                "count": 4
            },
            {
                "channel": 443,
                "count": 1
            },
            {
                "channel": 444,
                "count": 1
            },
            {
                "channel": 446,
                "count": 1
            },
            {
                "channel": 448,
                "count": 4
            },
            {
                "channel": 449,
                "count": 4
            },
            {
                "channel": 451,
                "count": 3
            },
            {
                "channel": 454,
                "count": 2
            },
            {
                "channel": 455,
                "count": 2
            },
            {
                "channel": 456,
                "count": 1
            },
            {
                "channel": 457,
                "count": 1
            },
            {
                "channel": 458,
                "count": 1
            },
            {
                "channel": 459,
                "count": 3
            },
            {
                "channel": 460,
                "count": 3
            },
            {
                "channel": 461,
                "count": 3
            },
            {
                "channel": 462,
                "count": 2
            },
            {
                "channel": 463,
                "count": 2
            },
            {
                "channel": 464,
                "count": 1
            },
            {
                "channel": 465,
                "count": 3
            },
            {
                "channel": 466,
                "count": 1
            },
            {
                "channel": 467,
                "count": 2
            },
            {
                "channel": 468,
                "count": 4
            },
            {
                "channel": 469,
                "count": 3
            },
            {
                "channel": 470,
                "count": 2
            },
            {
                "channel": 472,
                "count": 1
            },
            {
                "channel": 473,
                "count": 1
            },
            {
                "channel": 474,
                "count": 2
            },
            {
                "channel": 475,
                "count": 3
            },
            {
                "channel": 476,
                "count": 6
            },
            {
                "channel": 477,
                "count": 1
            },
            {
                "channel": 478,
                "count": 2
            },
            {
                "channel": 479,
                "count": 2
            },
            {
                "channel": 480,
                "count": 1
            },
            {
                "channel": 481,
                "count": 2
            },
            {
                "channel": 482,
                "count": 1
            },
            {
                "channel": 483,
                "count": 5
            },
            {
                "channel": 485,
                "count": 1
            },
            {
                "channel": 487,
                "count": 2
            },
            {
                "channel": 488,
                "count": 3
            },
            {
                "channel": 489,
                "count": 1
            },
            {
                "channel": 491,
                "count": 3
            },
            {
                "channel": 492,
                "count": 2
            },
            {
                "channel": 493,
                "count": 1
            },
            {
                "channel": 494,
                "count": 5
            },
            {
                "channel": 495,
                "count": 1
            },
            {
                "channel": 496,
                "count": 5
            },
            {
                "channel": 498,
                "count": 1
            },
            {
                "channel": 500,
                "count": 2
            },
            {
                "channel": 501,
                "count": 1
            },
            {
                "channel": 503,
                "count": 1
            },
            {
                "channel": 504,
                "count": 4
            },
            {
                "channel": 505,
                "count": 1
            },
            {
                "channel": 506,
                "count": 1
            },
            {
                "channel": 507,
                "count": 1
            },
            {
                "channel": 509,
                "count": 1
            },
            {
                "channel": 510,
                "count": 1
            },
            {
                "channel": 511,
                "count": 1
            },
            {
                "channel": 512,
                "count": 3
            }
        ],
        "run_id": "f230b5fdc7f95813c75828ec32270d2657d3c4f2",
        "segment_duration": 60,
        "segment_number": 1,
        "segment_type": "guppy-acquisition",
        "software": {
            "analysis": "1d_basecalling",
            "name": "guppy-basecalling",
            "version": "2.3.7+e041753"
        },
        "tracking_id": {
            "asic_id": "417755593",
            "asic_id_eeprom": "2595519",
            "asic_temp": "33.944931",
            "asic_version": "IA02D",
            "auto_update": "0",
            "auto_update_source": "https://mirror.oxfordnanoportal.com/software/MinKNOW/",
            "bream_is_standard": "0",
            "device_id": "MN19628",
            "device_type": "minion",
            "exp_script_name": "b17b89a985f80d3f9112f6d5bbeb69403ed3632f-2d2f7defe1969ba1b5d749ef5f1bb40e7622ffa3",
            "exp_script_purpose": "sequencing_run",
            "exp_start_time": "2018-12-11T14:57:44Z",
            "flow_cell_id": "FAK22428",
            "heatsink_temp": "33.019531",
            "hostname": "L0144169",
            "installation_type": "nc",
            "local_firmware_file": "1",
            "msg_id": "067c5e59-7e0d-44af-a686-123501062e69",
            "operating_system": "Windows 6.1",
            "protocol_run_id": "5da48d08-db99-4e4c-8c22-284de8fb1640",
            "protocols_version": "1.15.10.20",
            "run_id": "f230b5fdc7f95813c75828ec32270d2657d3c4f2",
            "sample_id": "16Srhizosphere",
            "time_stamp": "2019-03-28T19:13:46Z",
            "usb_config": "firm_1.2.3_ware#rbt_4.5.6_rbt#ctrl#USB3",
            "version": "1.15.6"
        }
    },
    {
        "aggregation": "segment",
        "analysis_id": "4e7ec2b8-beaf-49ab-9371-f6fd816dc7fc",
        "basecall_1d": {
            "exit_status_dist": {
                "fail:qscore_filter": 131,
                "pass": 2638
            },
            "qscore_dist_temp": [
                {
                    "count": 2,
                    "mean_qscore": 1.5
                },
                {
                    "count": 3,
                    "mean_qscore": 2.0
                },
                {
                    "count": 7,
                    "mean_qscore": 2.5
                },
                {
                    "count": 8,
                    "mean_qscore": 3.0
                },
                {
                    "count": 8,
                    "mean_qscore": 3.5
                },
                {
                    "count": 10,
                    "mean_qscore": 4.0
                },
                {
                    "count": 14,
                    "mean_qscore": 4.5
                },
                {
                    "count": 14,
                    "mean_qscore": 5.0
                },
                {
                    "count": 23,
                    "mean_qscore": 5.5
                },
                {
                    "count": 16,
                    "mean_qscore": 6.0
                },
                {
                    "count": 26,
                    "mean_qscore": 6.5
                },
                {
                    "count": 37,
                    "mean_qscore": 7.0
                },
                {
                    "count": 61,
                    "mean_qscore": 7.5
                },
                {
                    "count": 84,
                    "mean_qscore": 8.0
                },
                {
                    "count": 157,
                    "mean_qscore": 8.5
                },
                {
                    "count": 242,
                    "mean_qscore": 9.0
                },
                {
                    "count": 342,
                    "mean_qscore": 9.5
                },
                {
                    "count": 483,
                    "mean_qscore": 10.0
                },
                {
                    "count": 519,
                    "mean_qscore": 10.5
                },
                {
                    "count": 449,
                    "mean_qscore": 11.0
                },
                {
                    "count": 206,
                    "mean_qscore": 11.5
                },
                {
                    "count": 47,
                    "mean_qscore": 12.0
                },
                {
                    "count": 10,
                    "mean_qscore": 12.5
                },
                {
                    "count": 1,
                    "mean_qscore": 13.5
                }
            ],
            "qscore_sum_temp": {
                "count": 2769,
                "mean": 10.029447555542,
                "sum": 27771.541015625
            },
            "read_len_events_sum_temp": 11215559,
            "seq_len_bases_dist_temp": [
                {
                    "count": 194,
                    "length": 0.0
                },
                {
                    "count": 2550,
                    "length": 1000.0
                },
                {
                    "count": 7,
                    "length": 2000.0
                },
                {
                    "count": 18,
                    "length": 3000.0
                }
            ],
            "seq_len_bases_sum_temp": 4148058,
            "seq_len_events_dist_temp": [
                {
                    "count": 3,
                    "length": 0.0
                },
                {
                    "count": 128,
                    "length": 1000.0
                },
                {
                    "count": 100,
                    "length": 2000.0
                },
                {
                    "count": 763,
                    "length": 3000.0
                },
                {
                    "count": 1651,
                    "length": 4000.0
                },
                {
                    "count": 63,
                    "length": 5000.0
                },
                {
                    "count": 26,
                    "length": 6000.0
                },
                {
                    "count": 13,
                    "length": 7000.0
                },
                {
                    "count": 17,
                    "length": 8000.0
                },
                {
                    "count": 3,
                    "length": 9000.0
                },
                {
                    "count": 1,
                    "length": 10000.0
                },
                {
                    "count": 1,
                    "length": 24000.0
                }
            ],
            "speed_bases_per_second_dist_temp": [
                {
                    "count": 1,
                    "speed": 41.0
                },
                {
                    "count": 1,
                    "speed": 55.0
                },
                {
                    "count": 1,
                    "speed": 61.0
                },
                {
                    "count": 1,
                    "speed": 65.0
                },
                {
                    "count": 1,
                    "speed": 77.0
                },
                {
                    "count": 2,
                    "speed": 83.0
                },
                {
                    "count": 1,
                    "speed": 87.0
                },
                {
                    "count": 1,
                    "speed": 89.0
                },
                {
                    "count": 1,
                    "speed": 97.0
                },
                {
                    "count": 1,
                    "speed": 111.0
                },
                {
                    "count": 1,
                    "speed": 117.0
                },
                {
                    "count": 1,
                    "speed": 129.0
                },
                {
                    "count": 1,
                    "speed": 131.0
                },
                {
                    "count": 2,
                    "speed": 133.0
                },
                {
                    "count": 1,
                    "speed": 135.0
                },
                {
                    "count": 1,
                    "speed": 145.0
                },
                {
                    "count": 1,
                    "speed": 151.0
                },
                {
                    "count": 1,
                    "speed": 153.0
                },
                {
                    "count": 1,
                    "speed": 155.0
                },
                {
                    "count": 2,
                    "speed": 159.0
                },
                {
                    "count": 1,
                    "speed": 163.0
                },
                {
                    "count": 2,
                    "speed": 169.0
                },
                {
                    "count": 1,
                    "speed": 175.0
                },
                {
                    "count": 1,
                    "speed": 177.0
                },
                {
                    "count": 1,
                    "speed": 183.0
                },
                {
                    "count": 1,
                    "speed": 187.0
                },
                {
                    "count": 2,
                    "speed": 191.0
                },
                {
                    "count": 2,
                    "speed": 195.0
                },
                {
                    "count": 1,
                    "speed": 197.0
                },
                {
                    "count": 1,
                    "speed": 203.0
                },
                {
                    "count": 2,
                    "speed": 209.0
                },
                {
                    "count": 1,
                    "speed": 215.0
                },
                {
                    "count": 1,
                    "speed": 221.0
                },
                {
                    "count": 1,
                    "speed": 223.0
                },
                {
                    "count": 2,
                    "speed": 225.0
                },
                {
                    "count": 2,
                    "speed": 227.0
                },
                {
                    "count": 2,
                    "speed": 233.0
                },
                {
                    "count": 2,
                    "speed": 235.0
                },
                {
                    "count": 1,
                    "speed": 237.0
                },
                {
                    "count": 2,
                    "speed": 239.0
                },
                {
                    "count": 2,
                    "speed": 241.0
                },
                {
                    "count": 1,
                    "speed": 247.0
                },
                {
                    "count": 1,
                    "speed": 249.0
                },
                {
                    "count": 1,
                    "speed": 251.0
                },
                {
                    "count": 2,
                    "speed": 253.0
                },
                {
                    "count": 2,
                    "speed": 255.0
                },
                {
                    "count": 3,
                    "speed": 257.0
                },
                {
                    "count": 2,
                    "speed": 261.0
                },
                {
                    "count": 4,
                    "speed": 265.0
                },
                {
                    "count": 4,
                    "speed": 269.0
                },
                {
                    "count": 3,
                    "speed": 271.0
                },
                {
                    "count": 3,
                    "speed": 273.0
                },
                {
                    "count": 1,
                    "speed": 275.0
                },
                {
                    "count": 1,
                    "speed": 277.0
                },
                {
                    "count": 1,
                    "speed": 283.0
                },
                {
                    "count": 4,
                    "speed": 285.0
                },
                {
                    "count": 1,
                    "speed": 287.0
                },
                {
                    "count": 2,
                    "speed": 289.0
                },
                {
                    "count": 5,
                    "speed": 291.0
                },
                {
                    "count": 1,
                    "speed": 293.0
                },
                {
                    "count": 4,
                    "speed": 295.0
                },
                {
                    "count": 2,
                    "speed": 297.0
                },
                {
                    "count": 2,
                    "speed": 299.0
                },
                {
                    "count": 4,
                    "speed": 301.0
                },
                {
                    "count": 6,
                    "speed": 303.0
                },
                {
                    "count": 3,
                    "speed": 305.0
                },
                {
                    "count": 3,
                    "speed": 307.0
                },
                {
                    "count": 3,
                    "speed": 309.0
                },
                {
                    "count": 6,
                    "speed": 311.0
                },
                {
                    "count": 3,
                    "speed": 313.0
                },
                {
                    "count": 7,
                    "speed": 315.0
                },
                {
                    "count": 5,
                    "speed": 317.0
                },
                {
                    "count": 6,
                    "speed": 319.0
                },
                {
                    "count": 9,
                    "speed": 321.0
                },
                {
                    "count": 10,
                    "speed": 323.0
                },
                {
                    "count": 8,
                    "speed": 325.0
                },
                {
                    "count": 9,
                    "speed": 327.0
                },
                {
                    "count": 14,
                    "speed": 329.0
                },
                {
                    "count": 9,
                    "speed": 331.0
                },
                {
                    "count": 4,
                    "speed": 333.0
                },
                {
                    "count": 21,
                    "speed": 335.0
                },
                {
                    "count": 21,
                    "speed": 337.0
                },
                {
                    "count": 25,
                    "speed": 339.0
                },
                {
                    "count": 18,
                    "speed": 341.0
                },
                {
                    "count": 27,
                    "speed": 343.0
                },
                {
                    "count": 36,
                    "speed": 345.0
                },
                {
                    "count": 37,
                    "speed": 347.0
                },
                {
                    "count": 35,
                    "speed": 349.0
                },
                {
                    "count": 46,
                    "speed": 351.0
                },
                {
                    "count": 52,
                    "speed": 353.0
                },
                {
                    "count": 44,
                    "speed": 355.0
                },
                {
                    "count": 62,
                    "speed": 357.0
                },
                {
                    "count": 61,
                    "speed": 359.0
                },
                {
                    "count": 63,
                    "speed": 361.0
                },
                {
                    "count": 68,
                    "speed": 363.0
                },
                {
                    "count": 81,
                    "speed": 365.0
                },
                {
                    "count": 85,
                    "speed": 367.0
                },
                {
                    "count": 89,
                    "speed": 369.0
                },
                {
                    "count": 105,
                    "speed": 371.0
                },
                {
                    "count": 76,
                    "speed": 373.0
                },
                {
                    "count": 79,
                    "speed": 375.0
                },
                {
                    "count": 84,
                    "speed": 377.0
                },
                {
                    "count": 103,
                    "speed": 379.0
                },
                {
                    "count": 91,
                    "speed": 381.0
                },
                {
                    "count": 98,
                    "speed": 383.0
                },
                {
                    "count": 83,
                    "speed": 385.0
                },
                {
                    "count": 93,
                    "speed": 387.0
                },
                {
                    "count": 68,
                    "speed": 389.0
                },
                {
                    "count": 76,
                    "speed": 391.0
                },
                {
                    "count": 72,
                    "speed": 393.0
                },
                {
                    "count": 63,
                    "speed": 395.0
                },
                {
                    "count": 64,
                    "speed": 397.0
                },
                {
                    "count": 50,
                    "speed": 399.0
                },
                {
                    "count": 40,
                    "speed": 401.0
                },
                {
                    "count": 44,
                    "speed": 403.0
                },
                {
                    "count": 35,
                    "speed": 405.0
                },
                {
                    "count": 24,
                    "speed": 407.0
                },
                {
                    "count": 35,
                    "speed": 409.0
                },
                {
                    "count": 30,
                    "speed": 411.0
                },
                {
                    "count": 20,
                    "speed": 413.0
                },
                {
                    "count": 30,
                    "speed": 415.0
                },
                {
                    "count": 12,
                    "speed": 417.0
                },
                {
                    "count": 25,
                    "speed": 419.0
                },
                {
                    "count": 24,
                    "speed": 421.0
                },
                {
                    "count": 19,
                    "speed": 423.0
                },
                {
                    "count": 13,
                    "speed": 425.0
                },
                {
                    "count": 11,
                    "speed": 427.0
                },
                {
                    "count": 11,
                    "speed": 429.0
                },
                {
                    "count": 11,
                    "speed": 431.0
                },
                {
                    "count": 14,
                    "speed": 433.0
                },
                {
                    "count": 12,
                    "speed": 435.0
                },
                {
                    "count": 9,
                    "speed": 437.0
                },
                {
                    "count": 8,
                    "speed": 439.0
                },
                {
                    "count": 4,
                    "speed": 441.0
                },
                {
                    "count": 4,
                    "speed": 443.0
                },
                {
                    "count": 3,
                    "speed": 445.0
                },
                {
                    "count": 3,
                    "speed": 447.0
                },
                {
                    "count": 6,
                    "speed": 449.0
                },
                {
                    "count": 6,
                    "speed": 451.0
                },
                {
                    "count": 5,
                    "speed": 453.0
                },
                {
                    "count": 2,
                    "speed": 455.0
                },
                {
                    "count": 2,
                    "speed": 457.0
                },
                {
                    "count": 3,
                    "speed": 459.0
                },
                {
                    "count": 1,
                    "speed": 461.0
                },
                {
                    "count": 2,
                    "speed": 463.0
                },
                {
                    "count": 2,
                    "speed": 467.0
                },
                {
                    "count": 1,
                    "speed": 471.0
                },
                {
                    "count": 1,
                    "speed": 477.0
                },
                {
                    "count": 1,
                    "speed": 479.0
                },
                {
                    "count": 1,
                    "speed": 481.0
                },
                {
                    "count": 3,
                    "speed": 483.0
                },
                {
                    "count": 1,
                    "speed": 517.0
                },
                {
                    "count": 1,
                    "speed": 531.0
                },
                {
                    "count": 1,
                    "speed": 549.0
                }
            ],
            "strand_median_pa": {
                "count": 2769,
                "mean": 79.0457992553711,
                "sum": 218877.8125
            },
            "strand_sd_pa": {
                "count": 2769,
                "mean": 9.72172260284424,
                "sum": 26919.451171875
            }
        },
        "channel_count": 493,
        "context_tags": {
            "experiment_duration_set": "1440",
            "experiment_type": "genomic_dna",
            "fast5_output_fastq_in_hdf": "1",
            "fast5_raw": "1",
            "fast5_reads_per_folder": "4000",
            "fastq_enabled": "1",
            "fastq_reads_per_file": "4000",
            "filename": "l0144169_20181211_fak22428_mn19628_sequencing_run_16srhizosphere_23805",
            "flowcell_type": "flo-min106",
            "kit_classification": "none",
            "local_basecalling": "0",
            "local_bc_comp_model": "",
            "local_bc_temp_model": "template_r9.4_450bps_5mer_raw.jsn",
            "sample_frequency": "4000",
            "sequencing_kit": "sqk-rab204",
            "user_filename_input": "16srhizosphere"
        },
        "latest_run_time": 5206.94580078125,
        "levels_sums": {
            "count": 2769,
            "mean": 199.909530639648,
            "open_pore_level_sum": 553549.5
        },
        "opts": {
            "adapter_pt_range_scale": "5.200000",
            "builtin_scripts": "1",
            "calib_detect": "0",
            "calib_max_sequence_length": "3800",
            "calib_min_coverage": "0.600000",
            "calib_min_sequence_length": "3000",
            "calib_reference": "lambda_3.6kb.fasta",
            "chunk_size": "1000",
            "chunks_per_caller": "10000",
            "chunks_per_runner": "20",
            "cpu_threads_per_caller": "1",
            "device": "",
            "disable_events": "0",
            "disable_pings": "0",
            "dmean_threshold": "1.000000",
            "dmean_win_size": "2",
            "enable_trimming": "1",
            "fast5_out": "0",
            "flowcell": "FLO-MIN106",
            "gpu_runners_per_device": "20",
            "hp_correct": "1",
            "jump_threshold": "1.000000",
            "kernel_path": "",
            "kit": "SQK-RAB204",
            "max_search_len": "1000",
            "min_qscore": "7.000000",
            "model_file": "template_r9.4.1_450bps_fast_flipflop.jsn",
            "num_callers": "4",
            "overlap": "50",
            "override_scaling": "0",
            "ping_segment_duration": "60",
            "ping_url": "https://ping.oxfordnanoportal.com/basecall",
            "port": "",
            "print_workflows": "0",
            "pt_median_offset": "2.500000",
            "pt_minimum_read_start_index": "30",
            "pt_required_adapter_drop": "30.000000",
            "pt_scaling": "0",
            "qscore_filtering": "0",
            "qscore_offset": "-1.300000",
            "qscore_scale": "1.100000",
            "quiet": "0",
            "read_id_list": "",
            "records_per_fastq": "4000",
            "recursive": "0",
            "reverse_sequence": "0",
            "scaling_mad": "0.000000",
            "scaling_med": "0.000000",
            "stay_penalty": "1.000000",
            "temp_bias": "1.000000",
            "temp_weight": "1.000000",
            "trace_categories_logs": "",
            "trim_min_events": "3",
            "trim_strategy": "dna",
            "trim_threshold": "2.500000",
            "u_substitution": "0",
            "verbose_logs": "0"
        },
        "read_count": 2769,
        "reads_per_channel_dist": [
            {
                "channel": 1,
                "count": 6
            },
            {
                "channel": 2,
                "count": 3
            },
            {
                "channel": 3,
                "count": 2
            },
            {
                "channel": 4,
                "count": 5
            },
            {
                "channel": 5,
                "count": 7
            },
            {
                "channel": 6,
                "count": 2
            },
            {
                "channel": 7,
                "count": 6
            },
            {
                "channel": 8,
                "count": 5
            },
            {
                "channel": 9,
                "count": 9
            },
            {
                "channel": 10,
                "count": 5
            },
            {
                "channel": 11,
                "count": 8
            },
            {
                "channel": 12,
                "count": 1
            },
            {
                "channel": 13,
                "count": 7
            },
            {
                "channel": 14,
                "count": 4
            },
            {
                "channel": 15,
                "count": 6
            },
            {
                "channel": 16,
                "count": 6
            },
            {
                "channel": 17,
                "count": 8
            },
            {
                "channel": 18,
                "count": 2
            },
            {
                "channel": 19,
                "count": 5
            },
            {
                "channel": 20,
                "count": 8
            },
            {
                "channel": 21,
                "count": 7
            },
            {
                "channel": 22,
                "count": 4
            },
            {
                "channel": 23,
                "count": 4
            },
            {
                "channel": 24,
                "count": 10
            },
            {
                "channel": 25,
                "count": 6
            },
            {
                "channel": 26,
                "count": 7
            },
            {
                "channel": 27,
                "count": 4
            },
            {
                "channel": 28,
                "count": 8
            },
            {
                "channel": 29,
                "count": 7
            },
            {
                "channel": 30,
                "count": 8
            },
            {
                "channel": 31,
                "count": 9
            },
            {
                "channel": 32,
                "count": 4
            },
            {
                "channel": 33,
                "count": 4
            },
            {
                "channel": 34,
                "count": 4
            },
            {
                "channel": 35,
                "count": 10
            },
            {
                "channel": 36,
                "count": 2
            },
            {
                "channel": 37,
                "count": 5
            },
            {
                "channel": 38,
                "count": 9
            },
            {
                "channel": 39,
                "count": 9
            },
            {
                "channel": 41,
                "count": 1
            },
            {
                "channel": 42,
                "count": 6
            },
            {
                "channel": 43,
                "count": 8
            },
            {
                "channel": 44,
                "count": 3
            },
            {
                "channel": 45,
                "count": 1
            },
            {
                "channel": 46,
                "count": 4
            },
            {
                "channel": 47,
                "count": 4
            },
            {
                "channel": 48,
                "count": 5
            },
            {
                "channel": 49,
                "count": 4
            },
            {
                "channel": 50,
                "count": 4
            },
            {
                "channel": 52,
                "count": 2
            },
            {
                "channel": 53,
                "count": 3
            },
            {
                "channel": 54,
                "count": 3
            },
            {
                "channel": 55,
                "count": 1
            },
            {
                "channel": 56,
                "count": 3
            },
            {
                "channel": 57,
                "count": 6
            },
            {
                "channel": 58,
                "count": 5
            },
            {
                "channel": 59,
                "count": 8
            },
            {
                "channel": 60,
                "count": 5
            },
            {
                "channel": 61,
                "count": 6
            },
            {
                "channel": 62,
                "count": 7
            },
            {
                "channel": 63,
                "count": 4
            },
            {
                "channel": 64,
                "count": 4
            },
            {
                "channel": 65,
                "count": 12
            },
            {
                "channel": 66,
                "count": 6
            },
            {
                "channel": 67,
                "count": 7
            },
            {
                "channel": 68,
                "count": 3
            },
            {
                "channel": 69,
                "count": 6
            },
            {
                "channel": 70,
                "count": 5
            },
            {
                "channel": 71,
                "count": 2
            },
            {
                "channel": 72,
                "count": 4
            },
            {
                "channel": 73,
                "count": 3
            },
            {
                "channel": 74,
                "count": 8
            },
            {
                "channel": 75,
                "count": 5
            },
            {
                "channel": 76,
                "count": 8
            },
            {
                "channel": 77,
                "count": 4
            },
            {
                "channel": 78,
                "count": 5
            },
            {
                "channel": 79,
                "count": 5
            },
            {
                "channel": 80,
                "count": 2
            },
            {
                "channel": 81,
                "count": 7
            },
            {
                "channel": 82,
                "count": 11
            },
            {
                "channel": 83,
                "count": 7
            },
            {
                "channel": 84,
                "count": 10
            },
            {
                "channel": 85,
                "count": 7
            },
            {
                "channel": 86,
                "count": 4
            },
            {
                "channel": 87,
                "count": 8
            },
            {
                "channel": 88,
                "count": 4
            },
            {
                "channel": 89,
                "count": 7
            },
            {
                "channel": 90,
                "count": 2
            },
            {
                "channel": 91,
                "count": 4
            },
            {
                "channel": 92,
                "count": 5
            },
            {
                "channel": 93,
                "count": 4
            },
            {
                "channel": 94,
                "count": 5
            },
            {
                "channel": 95,
                "count": 4
            },
            {
                "channel": 96,
                "count": 2
            },
            {
                "channel": 97,
                "count": 2
            },
            {
                "channel": 98,
                "count": 6
            },
            {
                "channel": 99,
                "count": 4
            },
            {
                "channel": 100,
                "count": 9
            },
            {
                "channel": 101,
                "count": 6
            },
            {
                "channel": 102,
                "count": 5
            },
            {
                "channel": 103,
                "count": 3
            },
            {
                "channel": 104,
                "count": 10
            },
            {
                "channel": 105,
                "count": 1
            },
            {
                "channel": 106,
                "count": 9
            },
            {
                "channel": 107,
                "count": 8
            },
            {
                "channel": 108,
                "count": 4
            },
            {
                "channel": 110,
                "count": 5
            },
            {
                "channel": 111,
                "count": 5
            },
            {
                "channel": 112,
                "count": 5
            },
            {
                "channel": 113,
                "count": 7
            },
            {
                "channel": 114,
                "count": 6
            },
            {
                "channel": 115,
                "count": 3
            },
            {
                "channel": 116,
                "count": 2
            },
            {
                "channel": 117,
                "count": 5
            },
            {
                "channel": 118,
                "count": 6
            },
            {
                "channel": 119,
                "count": 2
            },
            {
                "channel": 120,
                "count": 3
            },
            {
                "channel": 121,
                "count": 8
            },
            {
                "channel": 122,
                "count": 4
            },
            {
                "channel": 123,
                "count": 4
            },
            {
                "channel": 124,
                "count": 3
            },
            {
                "channel": 125,
                "count": 5
            },
            {
                "channel": 126,
                "count": 3
            },
            {
                "channel": 127,
                "count": 8
            },
            {
                "channel": 128,
                "count": 11
            },
            {
                "channel": 129,
                "count": 5
            },
            {
                "channel": 130,
                "count": 6
            },
            {
                "channel": 131,
                "count": 3
            },
            {
                "channel": 132,
                "count": 6
            },
            {
                "channel": 133,
                "count": 4
            },
            {
                "channel": 134,
                "count": 9
            },
            {
                "channel": 135,
                "count": 7
            },
            {
                "channel": 136,
                "count": 7
            },
            {
                "channel": 137,
                "count": 4
            },
            {
                "channel": 138,
                "count": 5
            },
            {
                "channel": 139,
                "count": 4
            },
            {
                "channel": 140,
                "count": 6
            },
            {
                "channel": 141,
                "count": 6
            },
            {
                "channel": 142,
                "count": 7
            },
            {
                "channel": 143,
                "count": 6
            },
            {
                "channel": 144,
                "count": 6
            },
            {
                "channel": 145,
                "count": 4
            },
            {
                "channel": 146,
                "count": 6
            },
            {
                "channel": 147,
                "count": 5
            },
            {
                "channel": 148,
                "count": 7
            },
            {
                "channel": 149,
                "count": 9
            },
            {
                "channel": 150,
                "count": 6
            },
            {
                "channel": 151,
                "count": 6
            },
            {
                "channel": 152,
                "count": 8
            },
            {
                "channel": 153,
                "count": 5
            },
            {
                "channel": 154,
                "count": 3
            },
            {
                "channel": 155,
                "count": 10
            },
            {
                "channel": 156,
                "count": 6
            },
            {
                "channel": 157,
                "count": 2
            },
            {
                "channel": 158,
                "count": 7
            },
            {
                "channel": 159,
                "count": 7
            },
            {
                "channel": 160,
                "count": 3
            },
            {
                "channel": 161,
                "count": 2
            },
            {
                "channel": 162,
                "count": 6
            },
            {
                "channel": 163,
                "count": 8
            },
            {
                "channel": 164,
                "count": 9
            },
            {
                "channel": 165,
                "count": 4
            },
            {
                "channel": 166,
                "count": 9
            },
            {
                "channel": 167,
                "count": 8
            },
            {
                "channel": 168,
                "count": 6
            },
            {
                "channel": 169,
                "count": 7
            },
            {
                "channel": 170,
                "count": 11
            },
            {
                "channel": 171,
                "count": 4
            },
            {
                "channel": 172,
                "count": 7
            },
            {
                "channel": 174,
                "count": 6
            },
            {
                "channel": 175,
                "count": 4
            },
            {
                "channel": 176,
                "count": 3
            },
            {
                "channel": 177,
                "count": 7
            },
            {
                "channel": 178,
                "count": 9
            },
            {
                "channel": 179,
                "count": 4
            },
            {
                "channel": 180,
                "count": 2
            },
            {
                "channel": 181,
                "count": 8
            },
            {
                "channel": 182,
                "count": 6
            },
            {
                "channel": 183,
                "count": 7
            },
            {
                "channel": 184,
                "count": 2
            },
            {
                "channel": 185,
                "count": 3
            },
            {
                "channel": 186,
                "count": 6
            },
            {
                "channel": 187,
                "count": 1
            },
            {
                "channel": 188,
                "count": 6
            },
            {
                "channel": 190,
                "count": 3
            },
            {
                "channel": 191,
                "count": 6
            },
            {
                "channel": 192,
                "count": 5
            },
            {
                "channel": 193,
                "count": 8
            },
            {
                "channel": 194,
                "count": 6
            },
            {
                "channel": 195,
                "count": 5
            },
            {
                "channel": 196,
                "count": 11
            },
            {
                "channel": 197,
                "count": 11
            },
            {
                "channel": 198,
                "count": 7
            },
            {
                "channel": 199,
                "count": 5
            },
            {
                "channel": 200,
                "count": 7
            },
            {
                "channel": 201,
                "count": 1
            },
            {
                "channel": 202,
                "count": 6
            },
            {
                "channel": 203,
                "count": 8
            },
            {
                "channel": 204,
                "count": 9
            },
            {
                "channel": 205,
                "count": 7
            },
            {
                "channel": 206,
                "count": 5
            },
            {
                "channel": 207,
                "count": 11
            },
            {
                "channel": 208,
                "count": 3
            },
            {
                "channel": 209,
                "count": 6
            },
            {
                "channel": 210,
                "count": 8
            },
            {
                "channel": 211,
                "count": 5
            },
            {
                "channel": 212,
                "count": 8
            },
            {
                "channel": 213,
                "count": 3
            },
            {
                "channel": 214,
                "count": 5
            },
            {
                "channel": 215,
                "count": 6
            },
            {
                "channel": 216,
                "count": 4
            },
            {
                "channel": 217,
                "count": 5
            },
            {
                "channel": 218,
                "count": 8
            },
            {
                "channel": 219,
                "count": 6
            },
            {
                "channel": 220,
                "count": 11
            },
            {
                "channel": 221,
                "count": 3
            },
            {
                "channel": 222,
                "count": 7
            },
            {
                "channel": 223,
                "count": 8
            },
            {
                "channel": 224,
                "count": 6
            },
            {
                "channel": 225,
                "count": 6
            },
            {
                "channel": 226,
                "count": 3
            },
            {
                "channel": 227,
                "count": 4
            },
            {
                "channel": 228,
                "count": 6
            },
            {
                "channel": 229,
                "count": 4
            },
            {
                "channel": 230,
                "count": 10
            },
            {
                "channel": 231,
                "count": 5
            },
            {
                "channel": 232,
                "count": 4
            },
            {
                "channel": 233,
                "count": 6
            },
            {
                "channel": 234,
                "count": 9
            },
            {
                "channel": 235,
                "count": 6
            },
            {
                "channel": 236,
                "count": 6
            },
            {
                "channel": 237,
                "count": 10
            },
            {
                "channel": 238,
                "count": 5
            },
            {
                "channel": 239,
                "count": 4
            },
            {
                "channel": 240,
                "count": 13
            },
            {
                "channel": 241,
                "count": 5
            },
            {
                "channel": 242,
                "count": 4
            },
            {
                "channel": 243,
                "count": 10
            },
            {
                "channel": 244,
                "count": 5
            },
            {
                "channel": 245,
                "count": 6
            },
            {
                "channel": 246,
                "count": 7
            },
            {
                "channel": 247,
                "count": 4
            },
            {
                "channel": 248,
                "count": 6
            },
            {
                "channel": 249,
                "count": 3
            },
            {
                "channel": 250,
                "count": 2
            },
            {
                "channel": 251,
                "count": 6
            },
            {
                "channel": 252,
                "count": 4
            },
            {
                "channel": 253,
                "count": 7
            },
            {
                "channel": 254,
                "count": 9
            },
            {
                "channel": 255,
                "count": 7
            },
            {
                "channel": 257,
                "count": 7
            },
            {
                "channel": 258,
                "count": 5
            },
            {
                "channel": 259,
                "count": 4
            },
            {
                "channel": 260,
                "count": 9
            },
            {
                "channel": 261,
                "count": 3
            },
            {
                "channel": 262,
                "count": 6
            },
            {
                "channel": 263,
                "count": 5
            },
            {
                "channel": 264,
                "count": 4
            },
            {
                "channel": 265,
                "count": 4
            },
            {
                "channel": 266,
                "count": 6
            },
            {
                "channel": 267,
                "count": 5
            },
            {
                "channel": 268,
                "count": 12
            },
            {
                "channel": 269,
                "count": 6
            },
            {
                "channel": 270,
                "count": 7
            },
            {
                "channel": 271,
                "count": 5
            },
            {
                "channel": 272,
                "count": 5
            },
            {
                "channel": 274,
                "count": 6
            },
            {
                "channel": 275,
                "count": 10
            },
            {
                "channel": 276,
                "count": 4
            },
            {
                "channel": 277,
                "count": 8
            },
            {
                "channel": 278,
                "count": 4
            },
            {
                "channel": 279,
                "count": 10
            },
            {
                "channel": 280,
                "count": 8
            },
            {
                "channel": 281,
                "count": 5
            },
            {
                "channel": 282,
                "count": 4
            },
            {
                "channel": 283,
                "count": 6
            },
            {
                "channel": 284,
                "count": 6
            },
            {
                "channel": 285,
                "count": 11
            },
            {
                "channel": 286,
                "count": 5
            },
            {
                "channel": 288,
                "count": 7
            },
            {
                "channel": 289,
                "count": 5
            },
            {
                "channel": 290,
                "count": 6
            },
            {
                "channel": 291,
                "count": 6
            },
            {
                "channel": 292,
                "count": 4
            },
            {
                "channel": 293,
                "count": 5
            },
            {
                "channel": 294,
                "count": 3
            },
            {
                "channel": 295,
                "count": 2
            },
            {
                "channel": 296,
                "count": 9
            },
            {
                "channel": 297,
                "count": 7
            },
            {
                "channel": 298,
                "count": 8
            },
            {
                "channel": 299,
                "count": 7
            },
            {
                "channel": 300,
                "count": 3
            },
            {
                "channel": 301,
                "count": 7
            },
            {
                "channel": 302,
                "count": 3
            },
            {
                "channel": 304,
                "count": 8
            },
            {
                "channel": 305,
                "count": 4
            },
            {
                "channel": 306,
                "count": 7
            },
            {
                "channel": 307,
                "count": 9
            },
            {
                "channel": 308,
                "count": 8
            },
            {
                "channel": 309,
                "count": 4
            },
            {
                "channel": 310,
                "count": 6
            },
            {
                "channel": 311,
                "count": 7
            },
            {
                "channel": 312,
                "count": 5
            },
            {
                "channel": 313,
                "count": 9
            },
            {
                "channel": 314,
                "count": 6
            },
            {
                "channel": 315,
                "count": 5
            },
            {
                "channel": 316,
                "count": 6
            },
            {
                "channel": 317,
                "count": 2
            },
            {
                "channel": 318,
                "count": 5
            },
            {
                "channel": 319,
                "count": 8
            },
            {
                "channel": 320,
                "count": 3
            },
            {
                "channel": 321,
                "count": 6
            },
            {
                "channel": 322,
                "count": 7
            },
            {
                "channel": 323,
                "count": 4
            },
            {
                "channel": 324,
                "count": 5
            },
            {
                "channel": 325,
                "count": 6
            },
            {
                "channel": 326,
                "count": 3
            },
            {
                "channel": 327,
                "count": 2
            },
            {
                "channel": 328,
                "count": 7
            },
            {
                "channel": 329,
                "count": 2
            },
            {
                "channel": 330,
                "count": 2
            },
            {
                "channel": 331,
                "count": 3
            },
            {
                "channel": 332,
                "count": 5
            },
            {
                "channel": 333,
                "count": 1
            },
            {
                "channel": 334,
                "count": 3
            },
            {
                "channel": 335,
                "count": 5
            },
            {
                "channel": 336,
                "count": 6
            },
            {
                "channel": 337,
                "count": 4
            },
            {
                "channel": 339,
                "count": 7
            },
            {
                "channel": 340,
                "count": 6
            },
            {
                "channel": 341,
                "count": 4
            },
            {
                "channel": 342,
                "count": 6
            },
            {
                "channel": 343,
                "count": 8
            },
            {
                "channel": 344,
                "count": 1
            },
            {
                "channel": 345,
                "count": 4
            },
            {
                "channel": 346,
                "count": 10
            },
            {
                "channel": 347,
                "count": 2
            },
            {
                "channel": 348,
                "count": 6
            },
            {
                "channel": 349,
                "count": 5
            },
            {
                "channel": 350,
                "count": 6
            },
            {
                "channel": 351,
                "count": 9
            },
            {
                "channel": 352,
                "count": 4
            },
            {
                "channel": 353,
                "count": 5
            },
            {
                "channel": 354,
                "count": 3
            },
            {
                "channel": 355,
                "count": 5
            },
            {
                "channel": 356,
                "count": 6
            },
            {
                "channel": 357,
                "count": 7
            },
            {
                "channel": 358,
                "count": 4
            },
            {
                "channel": 359,
                "count": 7
            },
            {
                "channel": 360,
                "count": 6
            },
            {
                "channel": 361,
                "count": 4
            },
            {
                "channel": 362,
                "count": 3
            },
            {
                "channel": 363,
                "count": 8
            },
            {
                "channel": 364,
                "count": 5
            },
            {
                "channel": 366,
                "count": 4
            },
            {
                "channel": 367,
                "count": 9
            },
            {
                "channel": 368,
                "count": 7
            },
            {
                "channel": 369,
                "count": 4
            },
            {
                "channel": 370,
                "count": 10
            },
            {
                "channel": 371,
                "count": 6
            },
            {
                "channel": 373,
                "count": 3
            },
            {
                "channel": 374,
                "count": 9
            },
            {
                "channel": 376,
                "count": 7
            },
            {
                "channel": 377,
                "count": 6
            },
            {
                "channel": 378,
                "count": 7
            },
            {
                "channel": 379,
                "count": 2
            },
            {
                "channel": 380,
                "count": 9
            },
            {
                "channel": 381,
                "count": 3
            },
            {
                "channel": 382,
                "count": 8
            },
            {
                "channel": 383,
                "count": 9
            },
            {
                "channel": 384,
                "count": 6
            },
            {
                "channel": 385,
                "count": 4
            },
            {
                "channel": 386,
                "count": 2
            },
            {
                "channel": 387,
                "count": 5
            },
            {
                "channel": 388,
                "count": 7
            },
            {
                "channel": 389,
                "count": 4
            },
            {
                "channel": 390,
                "count": 4
            },
            {
                "channel": 391,
                "count": 4
            },
            {
                "channel": 392,
                "count": 4
            },
            {
                "channel": 393,
                "count": 1
            },
            {
                "channel": 394,
                "count": 8
            },
            {
                "channel": 396,
                "count": 7
            },
            {
                "channel": 397,
                "count": 9
            },
            {
                "channel": 398,
                "count": 5
            },
            {
                "channel": 400,
                "count": 8
            },
            {
                "channel": 401,
                "count": 6
            },
            {
                "channel": 402,
                "count": 5
            },
            {
                "channel": 403,
                "count": 3
            },
            {
                "channel": 404,
                "count": 6
            },
            {
                "channel": 405,
                "count": 7
            },
            {
                "channel": 406,
                "count": 4
            },
            {
                "channel": 407,
                "count": 6
            },
            {
                "channel": 408,
                "count": 6
            },
            {
                "channel": 409,
                "count": 7
            },
            {
                "channel": 410,
                "count": 7
            },
            {
                "channel": 411,
                "count": 8
            },
            {
                "channel": 412,
                "count": 4
            },
            {
                "channel": 413,
                "count": 6
            },
            {
                "channel": 414,
                "count": 6
            },
            {
                "channel": 415,
                "count": 5
            },
            {
                "channel": 417,
                "count": 9
            },
            {
                "channel": 418,
                "count": 5
            },
            {
                "channel": 419,
                "count": 5
            },
            {
                "channel": 420,
                "count": 8
            },
            {
                "channel": 421,
                "count": 4
            },
            {
                "channel": 422,
                "count": 3
            },
            {
                "channel": 423,
                "count": 6
            },
            {
                "channel": 424,
                "count": 4
            },
            {
                "channel": 425,
                "count": 3
            },
            {
                "channel": 426,
                "count": 4
            },
            {
                "channel": 427,
                "count": 5
            },
            {
                "channel": 428,
                "count": 2
            },
            {
                "channel": 429,
                "count": 8
            },
            {
                "channel": 430,
                "count": 3
            },
            {
                "channel": 431,
                "count": 10
            },
            {
                "channel": 432,
                "count": 8
            },
            {
                "channel": 433,
                "count": 4
            },
            {
                "channel": 434,
                "count": 9
            },
            {
                "channel": 435,
                "count": 8
            },
            {
                "channel": 436,
                "count": 7
            },
            {
                "channel": 437,
                "count": 7
            },
            {
                "channel": 439,
                "count": 4
            },
            {
                "channel": 440,
                "count": 6
            },
            {
                "channel": 441,
                "count": 11
            },
            {
                "channel": 442,
                "count": 3
            },
            {
                "channel": 443,
                "count": 1
            },
            {
                "channel": 444,
                "count": 4
            },
            {
                "channel": 445,
                "count": 5
            },
            {
                "channel": 446,
                "count": 2
            },
            {
                "channel": 447,
                "count": 8
            },
            {
                "channel": 448,
                "count": 10
            },
            {
                "channel": 449,
                "count": 5
            },
            {
                "channel": 450,
                "count": 5
            },
            {
                "channel": 451,
                "count": 3
            },
            {
                "channel": 452,
                "count": 6
            },
            {
                "channel": 453,
                "count": 3
            },
            {
                "channel": 454,
                "count": 5
            },
            {
                "channel": 455,
                "count": 6
            },
            {
                "channel": 456,
                "count": 3
            },
            {
                "channel": 457,
                "count": 2
            },
            {
                "channel": 458,
                "count": 11
            },
            {
                "channel": 459,
                "count": 1
            },
            {
                "channel": 460,
                "count": 9
            },
            {
                "channel": 461,
                "count": 9
            },
            {
                "channel": 462,
                "count": 8
            },
            {
                "channel": 463,
                "count": 7
            },
            {
                "channel": 464,
                "count": 9
            },
            {
                "channel": 465,
                "count": 5
            },
            {
                "channel": 466,
                "count": 7
            },
            {
                "channel": 467,
                "count": 7
            },
            {
                "channel": 468,
                "count": 8
            },
            {
                "channel": 469,
                "count": 6
            },
            {
                "channel": 470,
                "count": 2
            },
            {
                "channel": 471,
                "count": 5
            },
            {
                "channel": 472,
                "count": 12
            },
            {
                "channel": 473,
                "count": 3
            },
            {
                "channel": 474,
                "count": 5
            },
            {
                "channel": 475,
                "count": 10
            },
            {
                "channel": 476,
                "count": 3
            },
            {
                "channel": 477,
                "count": 8
            },
            {
                "channel": 478,
                "count": 4
            },
            {
                "channel": 479,
                "count": 4
            },
            {
                "channel": 480,
                "count": 4
            },
            {
                "channel": 481,
                "count": 2
            },
            {
                "channel": 482,
                "count": 3
            },
            {
                "channel": 485,
                "count": 4
            },
            {
                "channel": 486,
                "count": 3
            },
            {
                "channel": 487,
                "count": 6
            },
            {
                "channel": 488,
                "count": 4
            },
            {
                "channel": 489,
                "count": 5
            },
            {
                "channel": 490,
                "count": 6
            },
            {
                "channel": 491,
                "count": 5
            },
            {
                "channel": 492,
                "count": 3
            },
            {
                "channel": 493,
                "count": 5
            },
            {
                "channel": 494,
                "count": 5
            },
            {
                "channel": 495,
                "count": 7
            },
            {
                "channel": 496,
                "count": 6
            },
            {
                "channel": 497,
                "count": 2
            },
            {
                "channel": 498,
                "count": 8
            },
            {
                "channel": 499,
                "count": 7
            },
            {
                "channel": 500,
                "count": 5
            },
            {
                "channel": 501,
                "count": 5
            },
            {
                "channel": 502,
                "count": 3
            },
            {
                "channel": 503,
                "count": 4
            },
            {
                "channel": 504,
                "count": 5
            },
            {
                "channel": 505,
                "count": 5
            },
            {
                "channel": 506,
                "count": 5
            },
            {
                "channel": 507,
                "count": 6
            },
            {
                "channel": 508,
                "count": 6
            },
            {
                "channel": 509,
                "count": 8
            },
            {
                "channel": 510,
                "count": 9
            },
            {
                "channel": 511,
                "count": 5
            },
            {
                "channel": 512,
                "count": 8
            }
        ],
        "run_id": "f230b5fdc7f95813c75828ec32270d2657d3c4f2",
        "segment_duration": 60,
        "segment_number": 2,
        "segment_type": "guppy-acquisition",
        "software": {
            "analysis": "1d_basecalling",
            "name": "guppy-basecalling",
            "version": "2.3.7+e041753"
        },
        "tracking_id": {
            "asic_id": "417755593",
            "asic_id_eeprom": "2595519",
            "asic_temp": "33.944931",
            "asic_version": "IA02D",
            "auto_update": "0",
            "auto_update_source": "https://mirror.oxfordnanoportal.com/software/MinKNOW/",
            "bream_is_standard": "0",
            "device_id": "MN19628",
            "device_type": "minion",
            "exp_script_name": "b17b89a985f80d3f9112f6d5bbeb69403ed3632f-2d2f7defe1969ba1b5d749ef5f1bb40e7622ffa3",
            "exp_script_purpose": "sequencing_run",
            "exp_start_time": "2018-12-11T14:57:44Z",
            "flow_cell_id": "FAK22428",
            "heatsink_temp": "33.019531",
            "hostname": "L0144169",
            "installation_type": "nc",
            "local_firmware_file": "1",
            "msg_id": "2d269b3c-9926-4fe2-a690-9aa0459193c3",
            "operating_system": "Windows 6.1",
            "protocol_run_id": "5da48d08-db99-4e4c-8c22-284de8fb1640",
            "protocols_version": "1.15.10.20",
            "run_id": "f230b5fdc7f95813c75828ec32270d2657d3c4f2",
            "sample_id": "16Srhizosphere",
            "time_stamp": "2019-03-28T19:13:46Z",
            "usb_config": "firm_1.2.3_ware#rbt_4.5.6_rbt#ctrl#USB3",
            "version": "1.15.6"
        }
    },
    {
        "aggregation": "cumulative",
        "analysis_id": "4e7ec2b8-beaf-49ab-9371-f6fd816dc7fc",
        "basecall_1d": {
            "exit_status_dist": {
                "fail:qscore_filter": 175,
                "pass": 3519
            },
            "qscore_dist_temp": [
                {
                    "count": 2,
                    "mean_qscore": 1.5
                },
                {
                    "count": 5,
                    "mean_qscore": 2.0
                },
                {
                    "count": 11,
                    "mean_qscore": 2.5
                },
                {
                    "count": 9,
                    "mean_qscore": 3.0
                },
                {
                    "count": 12,
                    "mean_qscore": 3.5
                },
                {
                    "count": 12,
                    "mean_qscore": 4.0
                },
                {
                    "count": 16,
                    "mean_qscore": 4.5
                },
                {
                    "count": 18,
                    "mean_qscore": 5.0
                },
                {
                    "count": 28,
                    "mean_qscore": 5.5
                },
                {
                    "count": 23,
                    "mean_qscore": 6.0
                },
                {
                    "count": 39,
                    "mean_qscore": 6.5
                },
                {
                    "count": 53,
                    "mean_qscore": 7.0
                },
                {
                    "count": 91,
                    "mean_qscore": 7.5
                },
                {
                    "count": 105,
                    "mean_qscore": 8.0
                },
                {
                    "count": 201,
                    "mean_qscore": 8.5
                },
                {
                    "count": 301,
                    "mean_qscore": 9.0
                },
                {
                    "count": 465,
                    "mean_qscore": 9.5
                },
                {
                    "count": 639,
                    "mean_qscore": 10.0
                },
                {
                    "count": 716,
                    "mean_qscore": 10.5
                },
                {
                    "count": 597,
                    "mean_qscore": 11.0
                },
                {
                    "count": 272,
                    "mean_qscore": 11.5
                },
                {
                    "count": 61,
                    "mean_qscore": 12.0
                },
                {
                    "count": 16,
                    "mean_qscore": 12.5
                },
                {
                    "count": 1,
                    "mean_qscore": 13.0
                },
                {
                    "count": 1,
                    "mean_qscore": 13.5
                }
            ],
            "qscore_sum_temp": {
                "count": 3694,
                "mean": 10.0357208251953,
                "sum": 37071.953125
            },
            "read_len_events_sum_temp": 14757948,
            "seq_len_bases_dist_temp": [
                {
                    "count": 275,
                    "length": 0.0
                },
                {
                    "count": 3391,
                    "length": 1000.0
                },
                {
                    "count": 7,
                    "length": 2000.0
                },
                {
                    "count": 21,
                    "length": 3000.0
                }
            ],
            "seq_len_bases_sum_temp": 5506634,
            "seq_len_events_dist_temp": [
                {
                    "count": 9,
                    "length": 0.0
                },
                {
                    "count": 177,
                    "length": 1000.0
                },
                {
                    "count": 144,
                    "length": 2000.0
                },
                {
                    "count": 1170,
                    "length": 3000.0
                },
                {
                    "count": 2043,
                    "length": 4000.0
                },
                {
                    "count": 78,
                    "length": 5000.0
                },
                {
                    "count": 31,
                    "length": 6000.0
                },
                {
                    "count": 18,
                    "length": 7000.0
                },
                {
                    "count": 17,
                    "length": 8000.0
                },
                {
                    "count": 4,
                    "length": 9000.0
                },
                {
                    "count": 2,
                    "length": 10000.0
                },
                {
                    "count": 1,
                    "length": 24000.0
                }
            ],
            "speed_bases_per_second_dist_temp": [
                {
                    "count": 1,
                    "speed": 15.0
                },
                {
                    "count": 1,
                    "speed": 41.0
                },
                {
                    "count": 1,
                    "speed": 53.0
                },
                {
                    "count": 1,
                    "speed": 55.0
                },
                {
                    "count": 1,
                    "speed": 61.0
                },
                {
                    "count": 1,
                    "speed": 65.0
                },
                {
                    "count": 1,
                    "speed": 77.0
                },
                {
                    "count": 2,
                    "speed": 83.0
                },
                {
                    "count": 1,
                    "speed": 87.0
                },
                {
                    "count": 1,
                    "speed": 89.0
                },
                {
                    "count": 2,
                    "speed": 97.0
                },
                {
                    "count": 1,
                    "speed": 103.0
                },
                {
                    "count": 1,
                    "speed": 105.0
                },
                {
                    "count": 2,
                    "speed": 111.0
                },
                {
                    "count": 1,
                    "speed": 117.0
                },
                {
                    "count": 1,
                    "speed": 129.0
                },
                {
                    "count": 1,
                    "speed": 131.0
                },
                {
                    "count": 2,
                    "speed": 133.0
                },
                {
                    "count": 1,
                    "speed": 135.0
                },
                {
                    "count": 1,
                    "speed": 141.0
                },
                {
                    "count": 1,
                    "speed": 145.0
                },
                {
                    "count": 1,
                    "speed": 151.0
                },
                {
                    "count": 1,
                    "speed": 153.0
                },
                {
                    "count": 1,
                    "speed": 155.0
                },
                {
                    "count": 2,
                    "speed": 159.0
                },
                {
                    "count": 1,
                    "speed": 163.0
                },
                {
                    "count": 2,
                    "speed": 169.0
                },
                {
                    "count": 1,
                    "speed": 175.0
                },
                {
                    "count": 2,
                    "speed": 177.0
                },
                {
                    "count": 1,
                    "speed": 183.0
                },
                {
                    "count": 2,
                    "speed": 187.0
                },
                {
                    "count": 4,
                    "speed": 191.0
                },
                {
                    "count": 1,
                    "speed": 193.0
                },
                {
                    "count": 2,
                    "speed": 195.0
                },
                {
                    "count": 1,
                    "speed": 197.0
                },
                {
                    "count": 1,
                    "speed": 203.0
                },
                {
                    "count": 1,
                    "speed": 207.0
                },
                {
                    "count": 4,
                    "speed": 209.0
                },
                {
                    "count": 1,
                    "speed": 215.0
                },
                {
                    "count": 1,
                    "speed": 217.0
                },
                {
                    "count": 1,
                    "speed": 221.0
                },
                {
                    "count": 1,
                    "speed": 223.0
                },
                {
                    "count": 2,
                    "speed": 225.0
                },
                {
                    "count": 2,
                    "speed": 227.0
                },
                {
                    "count": 2,
                    "speed": 233.0
                },
                {
                    "count": 2,
                    "speed": 235.0
                },
                {
                    "count": 1,
                    "speed": 237.0
                },
                {
                    "count": 2,
                    "speed": 239.0
                },
                {
                    "count": 2,
                    "speed": 241.0
                },
                {
                    "count": 1,
                    "speed": 245.0
                },
                {
                    "count": 1,
                    "speed": 247.0
                },
                {
                    "count": 1,
                    "speed": 249.0
                },
                {
                    "count": 1,
                    "speed": 251.0
                },
                {
                    "count": 2,
                    "speed": 253.0
                },
                {
                    "count": 3,
                    "speed": 255.0
                },
                {
                    "count": 3,
                    "speed": 257.0
                },
                {
                    "count": 1,
                    "speed": 259.0
                },
                {
                    "count": 2,
                    "speed": 261.0
                },
                {
                    "count": 5,
                    "speed": 265.0
                },
                {
                    "count": 1,
                    "speed": 267.0
                },
                {
                    "count": 5,
                    "speed": 269.0
                },
                {
                    "count": 3,
                    "speed": 271.0
                },
                {
                    "count": 3,
                    "speed": 273.0
                },
                {
                    "count": 1,
                    "speed": 275.0
                },
                {
                    "count": 3,
                    "speed": 277.0
                },
                {
                    "count": 2,
                    "speed": 283.0
                },
                {
                    "count": 4,
                    "speed": 285.0
                },
                {
                    "count": 1,
                    "speed": 287.0
                },
                {
                    "count": 2,
                    "speed": 289.0
                },
                {
                    "count": 7,
                    "speed": 291.0
                },
                {
                    "count": 1,
                    "speed": 293.0
                },
                {
                    "count": 4,
                    "speed": 295.0
                },
                {
                    "count": 2,
                    "speed": 297.0
                },
                {
                    "count": 3,
                    "speed": 299.0
                },
                {
                    "count": 4,
                    "speed": 301.0
                },
                {
                    "count": 6,
                    "speed": 303.0
                },
                {
                    "count": 3,
                    "speed": 305.0
                },
                {
                    "count": 3,
                    "speed": 307.0
                },
                {
                    "count": 5,
                    "speed": 309.0
                },
                {
                    "count": 7,
                    "speed": 311.0
                },
                {
                    "count": 4,
                    "speed": 313.0
                },
                {
                    "count": 8,
                    "speed": 315.0
                },
                {
                    "count": 5,
                    "speed": 317.0
                },
                {
                    "count": 8,
                    "speed": 319.0
                },
                {
                    "count": 11,
                    "speed": 321.0
                },
                {
                    "count": 11,
                    "speed": 323.0
                },
                {
                    "count": 9,
                    "speed": 325.0
                },
                {
                    "count": 13,
                    "speed": 327.0
                },
                {
                    "count": 14,
                    "speed": 329.0
                },
                {
                    "count": 14,
                    "speed": 331.0
                },
                {
                    "count": 5,
                    "speed": 333.0
                },
                {
                    "count": 24,
                    "speed": 335.0
                },
                {
                    "count": 22,
                    "speed": 337.0
                },
                {
                    "count": 32,
                    "speed": 339.0
                },
                {
                    "count": 21,
                    "speed": 341.0
                },
                {
                    "count": 31,
                    "speed": 343.0
                },
                {
                    "count": 43,
                    "speed": 345.0
                },
                {
                    "count": 42,
                    "speed": 347.0
                },
                {
                    "count": 43,
                    "speed": 349.0
                },
                {
                    "count": 50,
                    "speed": 351.0
                },
                {
                    "count": 58,
                    "speed": 353.0
                },
                {
                    "count": 61,
                    "speed": 355.0
                },
                {
                    "count": 72,
                    "speed": 357.0
                },
                {
                    "count": 74,
                    "speed": 359.0
                },
                {
                    "count": 78,
                    "speed": 361.0
                },
                {
                    "count": 83,
                    "speed": 363.0
                },
                {
                    "count": 101,
                    "speed": 365.0
                },
                {
                    "count": 106,
                    "speed": 367.0
                },
                {
                    "count": 112,
                    "speed": 369.0
                },
                {
                    "count": 125,
                    "speed": 371.0
                },
                {
                    "count": 104,
                    "speed": 373.0
                },
                {
                    "count": 112,
                    "speed": 375.0
                },
                {
                    "count": 102,
                    "speed": 377.0
                },
                {
                    "count": 117,
                    "speed": 379.0
                },
                {
                    "count": 113,
                    "speed": 381.0
                },
                {
                    "count": 119,
                    "speed": 383.0
                },
                {
                    "count": 106,
                    "speed": 385.0
                },
                {
                    "count": 122,
                    "speed": 387.0
                },
                {
                    "count": 101,
                    "speed": 389.0
                },
                {
                    "count": 102,
                    "speed": 391.0
                },
                {
                    "count": 96,
                    "speed": 393.0
                },
                {
                    "count": 83,
                    "speed": 395.0
                },
                {
                    "count": 96,
                    "speed": 397.0
                },
                {
                    "count": 68,
                    "speed": 399.0
                },
                {
                    "count": 64,
                    "speed": 401.0
                },
                {
                    "count": 70,
                    "speed": 403.0
                },
                {
                    "count": 58,
                    "speed": 405.0
                },
                {
                    "count": 44,
                    "speed": 407.0
                },
                {
                    "count": 64,
                    "speed": 409.0
                },
                {
                    "count": 51,
                    "speed": 411.0
                },
                {
                    "count": 33,
                    "speed": 413.0
                },
                {
                    "count": 46,
                    "speed": 415.0
                },
                {
                    "count": 22,
                    "speed": 417.0
                },
                {
                    "count": 42,
                    "speed": 419.0
                },
                {
                    "count": 35,
                    "speed": 421.0
                },
                {
                    "count": 29,
                    "speed": 423.0
                },
                {
                    "count": 24,
                    "speed": 425.0
                },
                {
                    "count": 22,
                    "speed": 427.0
                },
                {
                    "count": 19,
                    "speed": 429.0
                },
                {
                    "count": 20,
                    "speed": 431.0
                },
                {
                    "count": 16,
                    "speed": 433.0
                },
                {
                    "count": 21,
                    "speed": 435.0
                },
                {
                    "count": 12,
                    "speed": 437.0
                },
                {
                    "count": 14,
                    "speed": 439.0
                },
                {
                    "count": 12,
                    "speed": 441.0
                },
                {
                    "count": 8,
                    "speed": 443.0
                },
                {
                    "count": 10,
                    "speed": 445.0
                },
                {
                    "count": 7,
                    "speed": 447.0
                },
                {
                    "count": 11,
                    "speed": 449.0
                },
                {
                    "count": 7,
                    "speed": 451.0
                },
                {
                    "count": 11,
                    "speed": 453.0
                },
                {
                    "count": 3,
                    "speed": 455.0
                },
                {
                    "count": 2,
                    "speed": 457.0
                },
                {
                    "count": 6,
                    "speed": 459.0
                },
                {
                    "count": 2,
                    "speed": 461.0
                },
                {
                    "count": 5,
                    "speed": 463.0
                },
                {
                    "count": 2,
                    "speed": 465.0
                },
                {
                    "count": 2,
                    "speed": 467.0
                },
                {
                    "count": 3,
                    "speed": 471.0
                },
                {
                    "count": 1,
                    "speed": 473.0
                },
                {
                    "count": 1,
                    "speed": 477.0
                },
                {
                    "count": 2,
                    "speed": 479.0
                },
                {
                    "count": 1,
                    "speed": 481.0
                },
                {
                    "count": 3,
                    "speed": 483.0
                },
                {
                    "count": 1,
                    "speed": 487.0
                },
                {
                    "count": 1,
                    "speed": 493.0
                },
                {
                    "count": 1,
                    "speed": 499.0
                },
                {
                    "count": 1,
                    "speed": 501.0
                },
                {
                    "count": 1,
                    "speed": 507.0
                },
                {
                    "count": 1,
                    "speed": 517.0
                },
                {
                    "count": 1,
                    "speed": 531.0
                },
                {
                    "count": 1,
                    "speed": 549.0
                }
            ],
            "strand_median_pa": {
                "count": 3694,
                "mean": 79.5720596313477,
                "sum": 293939.1875
            },
            "strand_sd_pa": {
                "count": 3694,
                "mean": 9.76617813110352,
                "sum": 36076.26171875
            }
        },
        "channel_count": 504,
        "context_tags": {
            "experiment_duration_set": "1440",
            "experiment_type": "genomic_dna",
            "fast5_output_fastq_in_hdf": "1",
            "fast5_raw": "1",
            "fast5_reads_per_folder": "4000",
            "fastq_enabled": "1",
            "fastq_reads_per_file": "4000",
            "filename": "l0144169_20181211_fak22428_mn19628_sequencing_run_16srhizosphere_23805",
            "flowcell_type": "flo-min106",
            "kit_classification": "none",
            "local_basecalling": "0",
            "local_bc_comp_model": "",
            "local_bc_temp_model": "template_r9.4_450bps_5mer_raw.jsn",
            "sample_frequency": "4000",
            "sequencing_kit": "sqk-rab204",
            "user_filename_input": "16srhizosphere"
        },
        "latest_run_time": 5206.94580078125,
        "levels_sums": {
            "count": 3694,
            "mean": 200.641906738281,
            "open_pore_level_sum": 741171.1875
        },
        "opts": {
            "adapter_pt_range_scale": "5.200000",
            "builtin_scripts": "1",
            "calib_detect": "0",
            "calib_max_sequence_length": "3800",
            "calib_min_coverage": "0.600000",
            "calib_min_sequence_length": "3000",
            "calib_reference": "lambda_3.6kb.fasta",
            "chunk_size": "1000",
            "chunks_per_caller": "10000",
            "chunks_per_runner": "20",
            "cpu_threads_per_caller": "1",
            "device": "",
            "disable_events": "0",
            "disable_pings": "0",
            "dmean_threshold": "1.000000",
            "dmean_win_size": "2",
            "enable_trimming": "1",
            "fast5_out": "0",
            "flowcell": "FLO-MIN106",
            "gpu_runners_per_device": "20",
            "hp_correct": "1",
            "jump_threshold": "1.000000",
            "kernel_path": "",
            "kit": "SQK-RAB204",
            "max_search_len": "1000",
            "min_qscore": "7.000000",
            "model_file": "template_r9.4.1_450bps_fast_flipflop.jsn",
            "num_callers": "4",
            "overlap": "50",
            "override_scaling": "0",
            "ping_segment_duration": "60",
            "ping_url": "https://ping.oxfordnanoportal.com/basecall",
            "port": "",
            "print_workflows": "0",
            "pt_median_offset": "2.500000",
            "pt_minimum_read_start_index": "30",
            "pt_required_adapter_drop": "30.000000",
            "pt_scaling": "0",
            "qscore_filtering": "0",
            "qscore_offset": "-1.300000",
            "qscore_scale": "1.100000",
            "quiet": "0",
            "read_id_list": "",
            "records_per_fastq": "4000",
            "recursive": "0",
            "reverse_sequence": "0",
            "scaling_mad": "0.000000",
            "scaling_med": "0.000000",
            "stay_penalty": "1.000000",
            "temp_bias": "1.000000",
            "temp_weight": "1.000000",
            "trace_categories_logs": "",
            "trim_min_events": "3",
            "trim_strategy": "dna",
            "trim_threshold": "2.500000",
            "u_substitution": "0",
            "verbose_logs": "0"
        },
        "read_count": 3694,
        "reads_per_channel_dist": [
            {
                "channel": 1,
                "count": 7
            },
            {
                "channel": 2,
                "count": 5
            },
            {
                "channel": 3,
                "count": 2
            },
            {
                "channel": 4,
                "count": 6
            },
            {
                "channel": 5,
                "count": 7
            },
            {
                "channel": 6,
                "count": 2
            },
            {
                "channel": 7,
                "count": 8
            },
            {
                "channel": 8,
                "count": 9
            },
            {
                "channel": 9,
                "count": 12
            },
            {
                "channel": 10,
                "count": 7
            },
            {
                "channel": 11,
                "count": 9
            },
            {
                "channel": 12,
                "count": 9
            },
            {
                "channel": 13,
                "count": 8
            },
            {
                "channel": 14,
                "count": 5
            },
            {
                "channel": 15,
                "count": 7
            },
            {
                "channel": 16,
                "count": 7
            },
            {
                "channel": 17,
                "count": 10
            },
            {
                "channel": 18,
                "count": 4
            },
            {
                "channel": 19,
                "count": 6
            },
            {
                "channel": 20,
                "count": 9
            },
            {
                "channel": 21,
                "count": 11
            },
            {
                "channel": 22,
                "count": 4
            },
            {
                "channel": 23,
                "count": 4
            },
            {
                "channel": 24,
                "count": 13
            },
            {
                "channel": 25,
                "count": 7
            },
            {
                "channel": 26,
                "count": 11
            },
            {
                "channel": 27,
                "count": 8
            },
            {
                "channel": 28,
                "count": 8
            },
            {
                "channel": 29,
                "count": 9
            },
            {
                "channel": 30,
                "count": 12
            },
            {
                "channel": 31,
                "count": 12
            },
            {
                "channel": 32,
                "count": 5
            },
            {
                "channel": 33,
                "count": 10
            },
            {
                "channel": 34,
                "count": 8
            },
            {
                "channel": 35,
                "count": 11
            },
            {
                "channel": 36,
                "count": 4
            },
            {
                "channel": 37,
                "count": 7
            },
            {
                "channel": 38,
                "count": 11
            },
            {
                "channel": 39,
                "count": 10
            },
            {
                "channel": 41,
                "count": 4
            },
            {
                "channel": 42,
                "count": 8
            },
            {
                "channel": 43,
                "count": 13
            },
            {
                "channel": 44,
                "count": 8
            },
            {
                "channel": 45,
                "count": 5
            },
            {
                "channel": 46,
                "count": 4
            },
            {
                "channel": 47,
                "count": 6
            },
            {
                "channel": 48,
                "count": 5
            },
            {
                "channel": 49,
                "count": 6
            },
            {
                "channel": 50,
                "count": 8
            },
            {
                "channel": 52,
                "count": 2
            },
            {
                "channel": 53,
                "count": 5
            },
            {
                "channel": 54,
                "count": 3
            },
            {
                "channel": 55,
                "count": 2
            },
            {
                "channel": 56,
                "count": 5
            },
            {
                "channel": 57,
                "count": 6
            },
            {
                "channel": 58,
                "count": 6
            },
            {
                "channel": 59,
                "count": 10
            },
            {
                "channel": 60,
                "count": 7
            },
            {
                "channel": 61,
                "count": 8
            },
            {
                "channel": 62,
                "count": 8
            },
            {
                "channel": 63,
                "count": 4
            },
            {
                "channel": 64,
                "count": 4
            },
            {
                "channel": 65,
                "count": 12
            },
            {
                "channel": 66,
                "count": 6
            },
            {
                "channel": 67,
                "count": 10
            },
            {
                "channel": 68,
                "count": 5
            },
            {
                "channel": 69,
                "count": 7
            },
            {
                "channel": 70,
                "count": 7
            },
            {
                "channel": 71,
                "count": 3
            },
            {
                "channel": 72,
                "count": 9
            },
            {
                "channel": 73,
                "count": 6
            },
            {
                "channel": 74,
                "count": 10
            },
            {
                "channel": 75,
                "count": 9
            },
            {
                "channel": 76,
                "count": 8
            },
            {
                "channel": 77,
                "count": 9
            },
            {
                "channel": 78,
                "count": 6
            },
            {
                "channel": 79,
                "count": 7
            },
            {
                "channel": 80,
                "count": 3
            },
            {
                "channel": 81,
                "count": 8
            },
            {
                "channel": 82,
                "count": 12
            },
            {
                "channel": 83,
                "count": 8
            },
            {
                "channel": 84,
                "count": 13
            },
            {
                "channel": 85,
                "count": 8
            },
            {
                "channel": 86,
                "count": 5
            },
            {
                "channel": 87,
                "count": 11
            },
            {
                "channel": 88,
                "count": 6
            },
            {
                "channel": 89,
                "count": 7
            },
            {
                "channel": 90,
                "count": 4
            },
            {
                "channel": 91,
                "count": 11
            },
            {
                "channel": 92,
                "count": 8
            },
            {
                "channel": 93,
                "count": 6
            },
            {
                "channel": 94,
                "count": 5
            },
            {
                "channel": 95,
                "count": 5
            },
            {
                "channel": 96,
                "count": 4
            },
            {
                "channel": 97,
                "count": 5
            },
            {
                "channel": 98,
                "count": 8
            },
            {
                "channel": 99,
                "count": 5
            },
            {
                "channel": 100,
                "count": 12
            },
            {
                "channel": 101,
                "count": 8
            },
            {
                "channel": 102,
                "count": 6
            },
            {
                "channel": 103,
                "count": 8
            },
            {
                "channel": 104,
                "count": 11
            },
            {
                "channel": 105,
                "count": 2
            },
            {
                "channel": 106,
                "count": 10
            },
            {
                "channel": 107,
                "count": 11
            },
            {
                "channel": 108,
                "count": 4
            },
            {
                "channel": 109,
                "count": 2
            },
            {
                "channel": 110,
                "count": 7
            },
            {
                "channel": 111,
                "count": 6
            },
            {
                "channel": 112,
                "count": 6
            },
            {
                "channel": 113,
                "count": 9
            },
            {
                "channel": 114,
                "count": 10
            },
            {
                "channel": 115,
                "count": 5
            },
            {
                "channel": 116,
                "count": 2
            },
            {
                "channel": 117,
                "count": 6
            },
            {
                "channel": 118,
                "count": 9
            },
            {
                "channel": 119,
                "count": 3
            },
            {
                "channel": 120,
                "count": 6
            },
            {
                "channel": 121,
                "count": 9
            },
            {
                "channel": 122,
                "count": 6
            },
            {
                "channel": 123,
                "count": 5
            },
            {
                "channel": 124,
                "count": 4
            },
            {
                "channel": 125,
                "count": 7
            },
            {
                "channel": 126,
                "count": 4
            },
            {
                "channel": 127,
                "count": 10
            },
            {
                "channel": 128,
                "count": 16
            },
            {
                "channel": 129,
                "count": 12
            },
            {
                "channel": 130,
                "count": 8
            },
            {
                "channel": 131,
                "count": 5
            },
            {
                "channel": 132,
                "count": 8
            },
            {
                "channel": 133,
                "count": 4
            },
            {
                "channel": 134,
                "count": 12
            },
            {
                "channel": 135,
                "count": 9
            },
            {
                "channel": 136,
                "count": 9
            },
            {
                "channel": 137,
                "count": 5
            },
            {
                "channel": 138,
                "count": 7
            },
            {
                "channel": 139,
                "count": 6
            },
            {
                "channel": 140,
                "count": 8
            },
            {
                "channel": 141,
                "count": 7
            },
            {
                "channel": 142,
                "count": 7
            },
            {
                "channel": 143,
                "count": 9
            },
            {
                "channel": 144,
                "count": 7
            },
            {
                "channel": 145,
                "count": 8
            },
            {
                "channel": 146,
                "count": 6
            },
            {
                "channel": 147,
                "count": 8
            },
            {
                "channel": 148,
                "count": 8
            },
            {
                "channel": 149,
                "count": 11
            },
            {
                "channel": 150,
                "count": 8
            },
            {
                "channel": 151,
                "count": 8
            },
            {
                "channel": 152,
                "count": 12
            },
            {
                "channel": 153,
                "count": 7
            },
            {
                "channel": 154,
                "count": 3
            },
            {
                "channel": 155,
                "count": 13
            },
            {
                "channel": 156,
                "count": 7
            },
            {
                "channel": 157,
                "count": 6
            },
            {
                "channel": 158,
                "count": 11
            },
            {
                "channel": 159,
                "count": 9
            },
            {
                "channel": 160,
                "count": 6
            },
            {
                "channel": 161,
                "count": 3
            },
            {
                "channel": 162,
                "count": 8
            },
            {
                "channel": 163,
                "count": 9
            },
            {
                "channel": 164,
                "count": 9
            },
            {
                "channel": 165,
                "count": 5
            },
            {
                "channel": 166,
                "count": 11
            },
            {
                "channel": 167,
                "count": 11
            },
            {
                "channel": 168,
                "count": 10
            },
            {
                "channel": 169,
                "count": 7
            },
            {
                "channel": 170,
                "count": 13
            },
            {
                "channel": 171,
                "count": 6
            },
            {
                "channel": 172,
                "count": 9
            },
            {
                "channel": 174,
                "count": 8
            },
            {
                "channel": 175,
                "count": 4
            },
            {
                "channel": 176,
                "count": 5
            },
            {
                "channel": 177,
                "count": 8
            },
            {
                "channel": 178,
                "count": 9
            },
            {
                "channel": 179,
                "count": 6
            },
            {
                "channel": 180,
                "count": 2
            },
            {
                "channel": 181,
                "count": 9
            },
            {
                "channel": 182,
                "count": 6
            },
            {
                "channel": 183,
                "count": 8
            },
            {
                "channel": 184,
                "count": 3
            },
            {
                "channel": 185,
                "count": 5
            },
            {
                "channel": 186,
                "count": 7
            },
            {
                "channel": 187,
                "count": 5
            },
            {
                "channel": 188,
                "count": 7
            },
            {
                "channel": 190,
                "count": 5
            },
            {
                "channel": 191,
                "count": 8
            },
            {
                "channel": 192,
                "count": 8
            },
            {
                "channel": 193,
                "count": 9
            },
            {
                "channel": 194,
                "count": 7
            },
            {
                "channel": 195,
                "count": 6
            },
            {
                "channel": 196,
                "count": 14
            },
            {
                "channel": 197,
                "count": 14
            },
            {
                "channel": 198,
                "count": 7
            },
            {
                "channel": 199,
                "count": 9
            },
            {
                "channel": 200,
                "count": 8
            },
            {
                "channel": 201,
                "count": 3
            },
            {
                "channel": 202,
                "count": 10
            },
            {
                "channel": 203,
                "count": 10
            },
            {
                "channel": 204,
                "count": 11
            },
            {
                "channel": 205,
                "count": 10
            },
            {
                "channel": 206,
                "count": 8
            },
            {
                "channel": 207,
                "count": 14
            },
            {
                "channel": 208,
                "count": 5
            },
            {
                "channel": 209,
                "count": 7
            },
            {
                "channel": 210,
                "count": 9
            },
            {
                "channel": 211,
                "count": 5
            },
            {
                "channel": 212,
                "count": 10
            },
            {
                "channel": 213,
                "count": 6
            },
            {
                "channel": 214,
                "count": 6
            },
            {
                "channel": 215,
                "count": 7
            },
            {
                "channel": 216,
                "count": 6
            },
            {
                "channel": 217,
                "count": 6
            },
            {
                "channel": 218,
                "count": 12
            },
            {
                "channel": 219,
                "count": 6
            },
            {
                "channel": 220,
                "count": 13
            },
            {
                "channel": 221,
                "count": 5
            },
            {
                "channel": 222,
                "count": 8
            },
            {
                "channel": 223,
                "count": 10
            },
            {
                "channel": 224,
                "count": 6
            },
            {
                "channel": 225,
                "count": 6
            },
            {
                "channel": 226,
                "count": 3
            },
            {
                "channel": 227,
                "count": 5
            },
            {
                "channel": 228,
                "count": 8
            },
            {
                "channel": 229,
                "count": 4
            },
            {
                "channel": 230,
                "count": 10
            },
            {
                "channel": 231,
                "count": 6
            },
            {
                "channel": 232,
                "count": 7
            },
            {
                "channel": 233,
                "count": 7
            },
            {
                "channel": 234,
                "count": 14
            },
            {
                "channel": 235,
                "count": 9
            },
            {
                "channel": 236,
                "count": 7
            },
            {
                "channel": 237,
                "count": 12
            },
            {
                "channel": 238,
                "count": 7
            },
            {
                "channel": 239,
                "count": 4
            },
            {
                "channel": 240,
                "count": 16
            },
            {
                "channel": 241,
                "count": 6
            },
            {
                "channel": 242,
                "count": 7
            },
            {
                "channel": 243,
                "count": 11
            },
            {
                "channel": 244,
                "count": 5
            },
            {
                "channel": 245,
                "count": 9
            },
            {
                "channel": 246,
                "count": 8
            },
            {
                "channel": 247,
                "count": 4
            },
            {
                "channel": 248,
                "count": 8
            },
            {
                "channel": 249,
                "count": 5
            },
            {
                "channel": 250,
                "count": 3
            },
            {
                "channel": 251,
                "count": 12
            },
            {
                "channel": 252,
                "count": 5
            },
            {
                "channel": 253,
                "count": 7
            },
            {
                "channel": 254,
                "count": 10
            },
            {
                "channel": 255,
                "count": 8
            },
            {
                "channel": 256,
                "count": 1
            },
            {
                "channel": 257,
                "count": 9
            },
            {
                "channel": 258,
                "count": 7
            },
            {
                "channel": 259,
                "count": 5
            },
            {
                "channel": 260,
                "count": 11
            },
            {
                "channel": 261,
                "count": 5
            },
            {
                "channel": 262,
                "count": 7
            },
            {
                "channel": 263,
                "count": 12
            },
            {
                "channel": 264,
                "count": 8
            },
            {
                "channel": 265,
                "count": 5
            },
            {
                "channel": 266,
                "count": 11
            },
            {
                "channel": 267,
                "count": 6
            },
            {
                "channel": 268,
                "count": 14
            },
            {
                "channel": 269,
                "count": 8
            },
            {
                "channel": 270,
                "count": 8
            },
            {
                "channel": 271,
                "count": 7
            },
            {
                "channel": 272,
                "count": 6
            },
            {
                "channel": 274,
                "count": 8
            },
            {
                "channel": 275,
                "count": 10
            },
            {
                "channel": 276,
                "count": 10
            },
            {
                "channel": 277,
                "count": 9
            },
            {
                "channel": 278,
                "count": 4
            },
            {
                "channel": 279,
                "count": 12
            },
            {
                "channel": 280,
                "count": 12
            },
            {
                "channel": 281,
                "count": 9
            },
            {
                "channel": 282,
                "count": 7
            },
            {
                "channel": 283,
                "count": 9
            },
            {
                "channel": 284,
                "count": 6
            },
            {
                "channel": 285,
                "count": 13
            },
            {
                "channel": 286,
                "count": 6
            },
            {
                "channel": 287,
                "count": 1
            },
            {
                "channel": 288,
                "count": 7
            },
            {
                "channel": 289,
                "count": 9
            },
            {
                "channel": 290,
                "count": 7
            },
            {
                "channel": 291,
                "count": 7
            },
            {
                "channel": 292,
                "count": 9
            },
            {
                "channel": 293,
                "count": 6
            },
            {
                "channel": 294,
                "count": 3
            },
            {
                "channel": 295,
                "count": 5
            },
            {
                "channel": 296,
                "count": 10
            },
            {
                "channel": 297,
                "count": 10
            },
            {
                "channel": 298,
                "count": 10
            },
            {
                "channel": 299,
                "count": 9
            },
            {
                "channel": 300,
                "count": 4
            },
            {
                "channel": 301,
                "count": 8
            },
            {
                "channel": 302,
                "count": 4
            },
            {
                "channel": 303,
                "count": 1
            },
            {
                "channel": 304,
                "count": 9
            },
            {
                "channel": 305,
                "count": 5
            },
            {
                "channel": 306,
                "count": 9
            },
            {
                "channel": 307,
                "count": 13
            },
            {
                "channel": 308,
                "count": 9
            },
            {
                "channel": 309,
                "count": 5
            },
            {
                "channel": 310,
                "count": 6
            },
            {
                "channel": 311,
                "count": 7
            },
            {
                "channel": 312,
                "count": 11
            },
            {
                "channel": 313,
                "count": 9
            },
            {
                "channel": 314,
                "count": 8
            },
            {
                "channel": 315,
                "count": 5
            },
            {
                "channel": 316,
                "count": 6
            },
            {
                "channel": 317,
                "count": 3
            },
            {
                "channel": 318,
                "count": 7
            },
            {
                "channel": 319,
                "count": 9
            },
            {
                "channel": 320,
                "count": 6
            },
            {
                "channel": 321,
                "count": 7
            },
            {
                "channel": 322,
                "count": 11
            },
            {
                "channel": 323,
                "count": 9
            },
            {
                "channel": 324,
                "count": 8
            },
            {
                "channel": 325,
                "count": 9
            },
            {
                "channel": 326,
                "count": 6
            },
            {
                "channel": 327,
                "count": 3
            },
            {
                "channel": 328,
                "count": 11
            },
            {
                "channel": 329,
                "count": 8
            },
            {
                "channel": 330,
                "count": 4
            },
            {
                "channel": 331,
                "count": 4
            },
            {
                "channel": 332,
                "count": 5
            },
            {
                "channel": 333,
                "count": 3
            },
            {
                "channel": 334,
                "count": 4
            },
            {
                "channel": 335,
                "count": 5
            },
            {
                "channel": 336,
                "count": 11
            },
            {
                "channel": 337,
                "count": 5
            },
            {
                "channel": 339,
                "count": 9
            },
            {
                "channel": 340,
                "count": 7
            },
            {
                "channel": 341,
                "count": 4
            },
            {
                "channel": 342,
                "count": 10
            },
            {
                "channel": 343,
                "count": 10
            },
            {
                "channel": 344,
                "count": 4
            },
            {
                "channel": 345,
                "count": 10
            },
            {
                "channel": 346,
                "count": 12
            },
            {
                "channel": 347,
                "count": 3
            },
            {
                "channel": 348,
                "count": 8
            },
            {
                "channel": 349,
                "count": 7
            },
            {
                "channel": 350,
                "count": 10
            },
            {
                "channel": 351,
                "count": 10
            },
            {
                "channel": 352,
                "count": 5
            },
            {
                "channel": 353,
                "count": 7
            },
            {
                "channel": 354,
                "count": 6
            },
            {
                "channel": 355,
                "count": 6
            },
            {
                "channel": 356,
                "count": 10
            },
            {
                "channel": 357,
                "count": 7
            },
            {
                "channel": 358,
                "count": 7
            },
            {
                "channel": 359,
                "count": 8
            },
            {
                "channel": 360,
                "count": 6
            },
            {
                "channel": 361,
                "count": 7
            },
            {
                "channel": 362,
                "count": 6
            },
            {
                "channel": 363,
                "count": 11
            },
            {
                "channel": 364,
                "count": 5
            },
            {
                "channel": 365,
                "count": 1
            },
            {
                "channel": 366,
                "count": 5
            },
            {
                "channel": 367,
                "count": 12
            },
            {
                "channel": 368,
                "count": 9
            },
            {
                "channel": 369,
                "count": 6
            },
            {
                "channel": 370,
                "count": 12
            },
            {
                "channel": 371,
                "count": 8
            },
            {
                "channel": 372,
                "count": 2
            },
            {
                "channel": 373,
                "count": 6
            },
            {
                "channel": 374,
                "count": 12
            },
            {
                "channel": 375,
                "count": 1
            },
            {
                "channel": 376,
                "count": 9
            },
            {
                "channel": 377,
                "count": 7
            },
            {
                "channel": 378,
                "count": 9
            },
            {
                "channel": 379,
                "count": 3
            },
            {
                "channel": 380,
                "count": 13
            },
            {
                "channel": 381,
                "count": 4
            },
            {
                "channel": 382,
                "count": 10
            },
            {
                "channel": 383,
                "count": 9
            },
            {
                "channel": 384,
                "count": 11
            },
            {
                "channel": 385,
                "count": 6
            },
            {
                "channel": 386,
                "count": 3
            },
            {
                "channel": 387,
                "count": 7
            },
            {
                "channel": 388,
                "count": 8
            },
            {
                "channel": 389,
                "count": 6
            },
            {
                "channel": 390,
                "count": 6
            },
            {
                "channel": 391,
                "count": 4
            },
            {
                "channel": 392,
                "count": 4
            },
            {
                "channel": 393,
                "count": 4
            },
            {
                "channel": 394,
                "count": 10
            },
            {
                "channel": 395,
                "count": 2
            },
            {
                "channel": 396,
                "count": 8
            },
            {
                "channel": 397,
                "count": 12
            },
            {
                "channel": 398,
                "count": 6
            },
            {
                "channel": 399,
                "count": 1
            },
            {
                "channel": 400,
                "count": 10
            },
            {
                "channel": 401,
                "count": 10
            },
            {
                "channel": 402,
                "count": 9
            },
            {
                "channel": 403,
                "count": 7
            },
            {
                "channel": 404,
                "count": 6
            },
            {
                "channel": 405,
                "count": 9
            },
            {
                "channel": 406,
                "count": 8
            },
            {
                "channel": 407,
                "count": 8
            },
            {
                "channel": 408,
                "count": 7
            },
            {
                "channel": 409,
                "count": 8
            },
            {
                "channel": 410,
                "count": 9
            },
            {
                "channel": 411,
                "count": 11
            },
            {
                "channel": 412,
                "count": 5
            },
            {
                "channel": 413,
                "count": 8
            },
            {
                "channel": 414,
                "count": 6
            },
            {
                "channel": 415,
                "count": 6
            },
            {
                "channel": 417,
                "count": 10
            },
            {
                "channel": 418,
                "count": 5
            },
            {
                "channel": 419,
                "count": 7
            },
            {
                "channel": 420,
                "count": 12
            },
            {
                "channel": 421,
                "count": 4
            },
            {
                "channel": 422,
                "count": 5
            },
            {
                "channel": 423,
                "count": 7
            },
            {
                "channel": 424,
                "count": 6
            },
            {
                "channel": 425,
                "count": 4
            },
            {
                "channel": 426,
                "count": 6
            },
            {
                "channel": 427,
                "count": 6
            },
            {
                "channel": 428,
                "count": 3
            },
            {
                "channel": 429,
                "count": 9
            },
            {
                "channel": 430,
                "count": 4
            },
            {
                "channel": 431,
                "count": 12
            },
            {
                "channel": 432,
                "count": 11
            },
            {
                "channel": 433,
                "count": 5
            },
            {
                "channel": 434,
                "count": 11
            },
            {
                "channel": 435,
                "count": 10
            },
            {
                "channel": 436,
                "count": 7
            },
            {
                "channel": 437,
                "count": 9
            },
            {
                "channel": 438,
                "count": 2
            },
            {
                "channel": 439,
                "count": 4
            },
            {
                "channel": 440,
                "count": 10
            },
            {
                "channel": 441,
                "count": 11
            },
            {
                "channel": 442,
                "count": 3
            },
            {
                "channel": 443,
                "count": 2
            },
            {
                "channel": 444,
                "count": 5
            },
            {
                "channel": 445,
                "count": 5
            },
            {
                "channel": 446,
                "count": 3
            },
            {
                "channel": 447,
                "count": 8
            },
            {
                "channel": 448,
                "count": 14
            },
            {
                "channel": 449,
                "count": 9
            },
            {
                "channel": 450,
                "count": 5
            },
            {
                "channel": 451,
                "count": 6
            },
            {
                "channel": 452,
                "count": 6
            },
            {
                "channel": 453,
                "count": 3
            },
            {
                "channel": 454,
                "count": 7
            },
            {
                "channel": 455,
                "count": 8
            },
            {
                "channel": 456,
                "count": 4
            },
            {
                "channel": 457,
                "count": 3
            },
            {
                "channel": 458,
                "count": 12
            },
            {
                "channel": 459,
                "count": 4
            },
            {
                "channel": 460,
                "count": 12
            },
            {
                "channel": 461,
                "count": 12
            },
            {
                "channel": 462,
                "count": 10
            },
            {
                "channel": 463,
                "count": 9
            },
            {
                "channel": 464,
                "count": 10
            },
            {
                "channel": 465,
                "count": 8
            },
            {
                "channel": 466,
                "count": 8
            },
            {
                "channel": 467,
                "count": 9
            },
            {
                "channel": 468,
                "count": 12
            },
            {
                "channel": 469,
                "count": 9
            },
            {
                "channel": 470,
                "count": 4
            },
            {
                "channel": 471,
                "count": 5
            },
            {
                "channel": 472,
                "count": 13
            },
            {
                "channel": 473,
                "count": 4
            },
            {
                "channel": 474,
                "count": 7
            },
            {
                "channel": 475,
                "count": 13
            },
            {
                "channel": 476,
                "count": 9
            },
            {
                "channel": 477,
                "count": 9
            },
            {
                "channel": 478,
                "count": 6
            },
            {
                "channel": 479,
                "count": 6
            },
            {
                "channel": 480,
                "count": 5
            },
            {
                "channel": 481,
                "count": 4
            },
            {
                "channel": 482,
                "count": 4
            },
            {
                "channel": 483,
                "count": 5
            },
            {
                "channel": 485,
                "count": 5
            },
            {
                "channel": 486,
                "count": 3
            },
            {
                "channel": 487,
                "count": 8
            },
            {
                "channel": 488,
                "count": 7
            },
            {
                "channel": 489,
                "count": 6
            },
            {
                "channel": 490,
                "count": 6
            },
            {
                "channel": 491,
                "count": 8
            },
            {
                "channel": 492,
                "count": 5
            },
            {
                "channel": 493,
                "count": 6
            },
            {
                "channel": 494,
                "count": 10
            },
            {
                "channel": 495,
                "count": 8
            },
            {
                "channel": 496,
                "count": 11
            },
            {
                "channel": 497,
                "count": 2
            },
            {
                "channel": 498,
                "count": 9
            },
            {
                "channel": 499,
                "count": 7
            },
            {
                "channel": 500,
                "count": 7
            },
            {
                "channel": 501,
                "count": 6
            },
            {
                "channel": 502,
                "count": 3
            },
            {
                "channel": 503,
                "count": 5
            },
            {
                "channel": 504,
                "count": 9
            },
            {
                "channel": 505,
                "count": 6
            },
            {
                "channel": 506,
                "count": 6
            },
            {
                "channel": 507,
                "count": 7
            },
            {
                "channel": 508,
                "count": 6
            },
            {
                "channel": 509,
                "count": 9
            },
            {
                "channel": 510,
                "count": 10
            },
            {
                "channel": 511,
                "count": 6
            },
            {
                "channel": 512,
                "count": 11
            }
        ],
        "run_id": "f230b5fdc7f95813c75828ec32270d2657d3c4f2",
        "segment_duration": 120,
        "segment_number": 1,
        "segment_type": "guppy-acquisition",
        "software": {
            "analysis": "1d_basecalling",
            "name": "guppy-basecalling",
            "version": "2.3.7+e041753"
        },
        "tracking_id": {
            "asic_id": "417755593",
            "asic_id_eeprom": "2595519",
            "asic_temp": "33.944931",
            "asic_version": "IA02D",
            "auto_update": "0",
            "auto_update_source": "https://mirror.oxfordnanoportal.com/software/MinKNOW/",
            "bream_is_standard": "0",
            "device_id": "MN19628",
            "device_type": "minion",
            "exp_script_name": "b17b89a985f80d3f9112f6d5bbeb69403ed3632f-2d2f7defe1969ba1b5d749ef5f1bb40e7622ffa3",
            "exp_script_purpose": "sequencing_run",
            "exp_start_time": "2018-12-11T14:57:44Z",
            "flow_cell_id": "FAK22428",
            "heatsink_temp": "33.019531",
            "hostname": "L0144169",
            "installation_type": "nc",
            "local_firmware_file": "1",
            "msg_id": "ec33d850-55e7-41a0-8167-64877e8f7b2d",
            "operating_system": "Windows 6.1",
            "protocol_run_id": "5da48d08-db99-4e4c-8c22-284de8fb1640",
            "protocols_version": "1.15.10.20",
            "run_id": "f230b5fdc7f95813c75828ec32270d2657d3c4f2",
            "sample_id": "16Srhizosphere",
            "time_stamp": "2019-03-28T19:13:46Z",
            "usb_config": "firm_1.2.3_ware#rbt_4.5.6_rbt#ctrl#USB3",
            "version": "1.15.6"
        }
    }
]