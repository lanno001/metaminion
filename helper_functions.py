import shutil
import os
from glob import glob
from datetime import datetime
import pathlib


def parse_output_dir(out_dir, clean=False):
    out_dir = os.path.abspath(out_dir) + '/'
    if clean:
        shutil.rmtree(out_dir, ignore_errors=True)
    pathlib.Path(out_dir).mkdir(parents=True, exist_ok=True)
    return out_dir


def parse_input_dir(in_dir, pattern=None):
    if type(in_dir) != list: in_dir = [in_dir]
    out_list = []
    for ind in in_dir:
        if not os.path.exists(ind):
            raise ValueError(f'{ind} does not exist')
        if os.path.isdir(ind):
            ind = os.path.abspath(ind)
            if pattern is None: out_list.extend(glob(f'{ind}/**/*', recursive=True))
            else: out_list.extend(glob(f'{ind}/**/{pattern}', recursive=True))
        else:
            if pattern is None: out_list.append(ind)
            elif pattern.strip('*') in ind: out_list.append(ind)
    return out_list


def parse_direct_dir(in_dir, pattern=None, paired=False):
    """
    Only return files matching pattern directly in in_dir, without path
    """
    if not os.path.exists(in_dir):
        raise ValueError(f'{in_dir} does not exist')
    if not os.path.isdir(in_dir):
        raise ValueError(f'{in_dir} is not a directory')
    in_dir = os.path.abspath(in_dir)
    if pattern is None:
        out_list = glob(f'{in_dir}/*', recursive=False)
    else:
        out_list = glob(f'{in_dir}/{pattern}', recursive=False)
    if paired:
        out_list = [ol.replace('_R1_', '_') for ol in out_list]
        out_list = set([ol.replace('_R2_', '_') for ol in out_list])
    return [os.path.basename(cf).split('.')[0] for cf in out_list]

def print_timestamp():
    return datetime.now().strftime('%y-%m-%d_%H:%M:%S')
