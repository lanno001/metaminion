from Bio import Entrez
from ete3 import NCBITaxa
import argparse
from os.path import splitext, isfile
from os import unlink
from helper_functions import parse_output_dir

from helper_functions import print_timestamp

parser = argparse.ArgumentParser(description='Construct a feature table tsv from NCBI fasta '
                                             'that is accepted by qiime2.')
parser.add_argument('--in-fasta', type=str, required=True,
                    help='fasta file containing reads with only accession id as header')
parser.add_argument('--entrez-email', type=str, required=True,
                    help='Your email address, used by ncbi to warn you if you are hogging resources')
parser.add_argument('--out-dir', type=str, required=True, help='output directory')
args = parser.parse_args()

out_dir = parse_output_dir(args.out_dir, clean=True)
table_fn = f'{out_dir}tax_table.tsv'
failed_fn = f'{out_dir}reads_failed.txt'
fasta_fn = f'{out_dir}reads.fasta'
fasta_taxid_fn = f'{out_dir}reads_taxid.fasta'
taxid_fn = f'{out_dir}taxa.txt'

ncbi = NCBITaxa()
Entrez.email = args.entrez_email

saved_ranks = {'superkingdom': 'su__', 'kingdom': 'k__', 'phylum': 'p__', 'class': 'c__',
               'order': 'o__', 'family': 'f__', 'genus': 'g__', 'species': 's__'}
skip_entry = False
print_threshold = 20
print_idx = 0

if isfile(failed_fn): unlink(failed_fn)
with open(args.in_fasta, 'r') as fh, \
        open(fasta_fn, 'w', buffering=8192) as foh, \
        open(table_fn, 'w', buffering=8192) as toh, \
        open(taxid_fn, 'w', buffering=8192) as tih, \
        open(fasta_taxid_fn, 'w', buffering=8192) as ftoh:
    for line in fh.readlines():
        if line[0] == '>':
            skip_entry = False
            header = line.split()[0]
            acc_id = header[1:]

            # --- retrieve tax id of accession ---
            with Entrez.elink(dbfrom='nuccore', id=acc_id, db='taxonomy') as elh:
                try:
                    tax_id = Entrez.read(elh)[0]['LinkSetDb'][0]['Link'][0]['Id']
                except:
                    print(f'could not retrieve taxonomy for accession {acc_id}, skipping...')
                    with open(failed_fn, 'a', buffering=8192) as failh:
                        failh.write(f'{str(acc_id)}\n')
                    skip_entry = True
                    continue

            # --- retrieve selected steps of lineage ---
            lineage_ids = ncbi.get_taxid_translator(ncbi.get_lineage(tax_id))
            lineage_ranks = ncbi.get_rank(lineage_ids)
            lineage_names = ncbi.get_taxid_translator(lineage_ids)
            lineage_dict = saved_ranks.copy()
            for lid in lineage_ranks:
                cur_lr = lineage_ranks[lid]
                if lineage_dict.get(cur_lr, False):
                    lineage_dict[lineage_ranks[lid]] += lineage_names[lid].split()[-1]

            lineage_str = f'{acc_id}\t{"; ".join(lineage_dict.values())}\n'
            toh.write(lineage_str)
            foh.write(header+'\n')
            ftoh.write(f'>{tax_id}\n')
            tih.write(str(tax_id)+'\n')
            print_idx += 1
            if print_idx > print_threshold:
                print(f'{print_timestamp()} {print_idx} entries retrieved...')
                print_threshold += 20
        else:
            if not skip_entry:
                foh.write(line)
                ftoh.write(line)
