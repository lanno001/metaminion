from Bio import Entrez
import argparse
from os.path import isfile
from os import unlink
from helper_functions import parse_output_dir
from ftplib import FTP
import tarfile

from helper_functions import print_timestamp

parser = argparse.ArgumentParser(description='Collect and store all NCBI resources required to build a centrifuge'
                                             'index.')
parser.add_argument('--in-fasta', type=str, required=True,
                    help='fasta file containing reads with only accession id as header')
parser.add_argument('--entrez-email', type=str, required=True,
                    help='Your email address, used by ncbi to warn you if you are hogging resources')
parser.add_argument('--out-dir', type=str, required=True, help='output directory')
args = parser.parse_args()

out_dir = parse_output_dir(args.out_dir, clean=True)
table_fn = f'{out_dir}conversion_table.tsv'
failed_fn = f'{out_dir}reads_failed.txt'
fasta_fn = f'{out_dir}reads.fasta'

Entrez.email = "args.entrez_email"

skip_entry = False
print_threshold = 20
print_idx = 0

if isfile(failed_fn): unlink(failed_fn)

# --- construct conversion table, clean fasta names ---
with open(args.in_fasta, 'r') as fh, \
        open(fasta_fn, 'w', buffering=8192) as foh, \
        open(table_fn, 'w', buffering=8192) as toh:
    for line in fh.readlines():
        if line[0] == '>':
            skip_entry = False
            header = line.split()[0]
            acc_id = header[1:]

            # --- retrieve tax id of accession ---
            with Entrez.elink(dbfrom='nuccore', id=acc_id, db='taxonomy') as elh:
                try:
                    tax_id = Entrez.read(elh)[0]['LinkSetDb'][0]['Link'][0]['Id']
                except:
                    print(f'could not retrieve taxonomy for accession {acc_id}, skipping...')
                    with open(failed_fn, 'a', buffering=8192) as failh:
                        failh.write(f'{str(acc_id)}\n')
                    skip_entry = True
                    continue

            # --- write ---
            toh.write(f'{acc_id}\t{tax_id}\n')
            foh.write(header+'\n')
            print_idx += 1
            if print_idx > print_threshold:
                print(f'{print_timestamp()} {print_idx} entries retrieved...')
                print_threshold += 20
        else:
            if not skip_entry: foh.write(line)

# --- download additional sources from NCBI ---
ftp = FTP("ftp.ncbi.nlm.nih.gov")
ftp.login()
taxdump_fn = f'{out_dir}/taxdump.tar.gz'
with open(taxdump_fn, 'wb') as tfh:
    ftp.retrbinary('RETR pub/taxonomy/taxdump.tar.gz', tfh.write)
ftp.close()

with tarfile.open(taxdump_fn) as th:
    th.extract('nodes.dmp', out_dir)
    th.extract('names.dmp', out_dir)
unlink(taxdump_fn)
