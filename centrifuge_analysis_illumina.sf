from os.path import splitext, basename
from helper_functions import parse_input_dir, parse_output_dir, parse_direct_dir
import os
import re

fastq_dir = config["fastq_dir"]  # query fastq file(s) directory/ies
fastq_list= parse_direct_dir(fastq_dir, pattern='*.fastq.gz', paired=True)
centrifuge_db = config["centrifuge_db"]  # centrifuge database
out_dir = parse_output_dir(config["out_dir"], clean=True) # output directory
metadata = config["metadata"] # qiime2 format meta data file for samples, may also contain metadata for absent samples
script_location = config["script_location"]  # location of snakemake file
if script_location[-1] != '/': script_location += '/'

__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))

rule target:
    input:
        bar_qzv=f'{out_dir}reads_bar.qzv'

rule make_sample_sheet:
    input:
        fastq_dir=fastq_dir
    params:
        out_dir=out_dir
    output:
        sample_sheet=f'{out_dir}sample_sheet.tsv'
    run:
        fastq_fn_list = parse_input_dir(input.fastq_dir, pattern='*.fastq.gz')
        fw_pattern = re.compile('_R1_[^/]+$')
        with open(output.sample_sheet, 'w') as rmh:
            for fw_fn in filter(fw_pattern.search, fastq_fn_list):
                bw_fn = fw_fn.replace('_R1_', '_R2_')
                if not bw_fn in fastq_fn_list: continue
                sample_name = splitext(splitext(basename(fw_fn.replace('_R1_', '_')))[0])[0]
                rmh.write(f'2\t{fw_fn}\t{bw_fn}\t{out_dir}centrifuge_classification_{sample_name}.tsv\t{out_dir}centrifuge_report_{sample_name}.tsv\n')

rule centrifuge:
    input:
        sample_sheet=f'{out_dir}sample_sheet.tsv'
    params:
        centrifuge_db=centrifuge_db
    output:
        report=expand(f'{out_dir}centrifuge_report_{{fastq}}.tsv', fastq=fastq_list),
          classification_tsv=expand(f'{out_dir}centrifuge_classification_{{fastq}}.tsv', fastq=fastq_list)
        # report=f'{out_dir}centrifuge_report_{{fastq}}.tsv',
        # classification_tsv=f'{out_dir}centrifuge_classification_{{fastq}}.tsv'
    threads: 1
    shell:
        """
        centrifuge \\
            -p {threads} \\
            -x {params.centrifuge_db} \\
            --sample-sheet {input.sample_sheet}
        """


rule make_kraken_report:
    input:
        classification_tsv=f'{out_dir}centrifuge_classification_{{fastq}}.tsv'
    params:
        centrifuge_db=centrifuge_db
    output:
        kraken_report=f'{out_dir}{{fastq}}'
    shell:
        """
        centrifuge-kreport \\
            -x {params.centrifuge_db} \\
            {input.classification_tsv} > {output.kraken_report}
        """

rule translate_classification:
    input:
        classification_tsv=expand(f'{out_dir}{{fastq}}', fastq=fastq_list)
    params:
        script_location=script_location
    output:
        classification_tsv=f'{out_dir}qiime2_classification_all.tsv'
    shell:
         """
         python {params.script_location}kreport2qiime.py \\
            --in-kreports {input.classification_tsv} \\
            --out-class {output.classification_tsv}
         """

rule import_classification:
    input:
        classification_tsv=f'{out_dir}qiime2_classification_all.tsv'
    output:
        classification_qza=f'{out_dir}qiime2_classification_all.qza'
    shell:
        """
        qiime tools import \\
            --type 'FeatureData[Taxonomy]' \\
            --input-path {input.classification_tsv} \\
            --output-path {output.classification_qza}
        """

rule make_biom:
    input:
        kraken_report=expand(f'{out_dir}{{fastq}}', fastq=fastq_list)
    output:
        biom=f'{out_dir}kraken_report.biom'
    shell:
        """
        kraken-biom --max G -o {output.biom} {input.kraken_report}
        """

rule import_biom:
    input:
        biom=f'{out_dir}kraken_report.biom'
    output:
        freq_qza=f'{out_dir}freq_table.qza'
    shell:
        """
        qiime tools import \\
            --type 'FeatureTable[Frequency]' \\
            --input-path {input.biom} \\
            --output-path {output.freq_qza}
        """


rule visualize_classification:
    input:
        class_qza=f'{out_dir}qiime2_classification_all.qza',
        metadata=metadata,
        freq_table=f'{out_dir}freq_table.qza'

    output:
        bar_qzv=f'{out_dir}reads_bar.qzv'
    shell:
         """
         qiime taxa barplot \\
            --i-table {input.freq_table} \\
            --i-taxonomy {input.class_qza} \\
            --m-metadata-file {input.metadata} \\
            --o-visualization {output.bar_qzv} 
         """
