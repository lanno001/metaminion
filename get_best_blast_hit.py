from Bio import SeqIO
from Bio.Blast import NCBIXML
from Bio.Blast.Applications import NcbiblastnCommandline
from helper_functions import parse_input_dir, parse_output_dir, print_timestamp
import os
import argparse
import re
import pandas as pd
from itertools import chain
import seaborn as sns
import matplotlib.pyplot as plt

parser = argparse.ArgumentParser(description='Blast fastas against a local blast database, return'
                                             'most likely organism per sequence.')
parser.add_argument('--db', type=str, required=True,
                    help='name of blast db')
parser.add_argument('--in-dir', type=str, required=True, nargs='+',
                    help='input directory containing fasta files, 1 per sample')
parser.add_argument('--out-dir', type=str,
                    help='dir where results are stored (created if non-existent).')
parser.add_argument('--cutoff', type=int, default=5,
                    help='define minimum number of reads for species to be considered identified')
parser.add_argument('--mock-composition', type=str, default=None,
                    help='define minimum number of reads for species to be considered identified')
args = parser.parse_args()

fasta_list = parse_input_dir(args.in_dir, '*.fasta')
outdir = parse_output_dir(args.out_dir, clean=True)

outdir_fasta = parse_output_dir(f'{outdir}fasta/')
outdir_xml = parse_output_dir(f'{outdir}xml/')
outdir_results = parse_output_dir(f'{outdir}results/')
failed_fn = outdir+'failed_reads.txt'

bp_df_list = []

pat = re.compile('[^a-zA-Z ]+')

try:
    with open(args.mock_composition, 'r') as mc:
        mock_list = mc.readlines()
    mock_list = [ml.replace('\n', '') for ml in mock_list]
except:
    mock_list = []

for fq_fn in fasta_list:
    fq_fn_clean = os.path.basename(os.path.splitext(fq_fn)[0])
    print(f'{print_timestamp()} processing sample {fq_fn_clean}')
    species_dict = dict()
    multi_fasta = SeqIO.parse(fq_fn, 'fasta')

    for fid, fasta in  enumerate(multi_fasta):
        new_fn = f'{outdir_fasta}{fq_fn_clean}_{fid}.fasta'
        new_xn = f'{outdir_xml}{fq_fn_clean}_{fid}.xml'
        SeqIO.write(fasta, new_fn, 'fasta')
        blastn_line = NcbiblastnCommandline(query=new_fn, db=args.db, evalue=0.00000000001,
                                            outfmt=5, out=new_xn, task='megablast')
        _ = blastn_line()
        with open(new_xn, 'r') as xfh:
            hr = NCBIXML.read(xfh)
            if not len(hr.alignments):
                with open(failed_fn, 'a') as failed_fh:
                    failed_fh.write(f'{fq_fn_clean}\n')
                continue
            hit_def = re.sub(pat, '', hr.alignments[0].hit_def)
            genus, species = hit_def.split(' ')[:2]  # todo: Under the contentious assumption that the first two words are genus and species
            tmp_dict = species_dict.get(genus, dict())
            tmp_dict[species] = tmp_dict.get(species, 0) + 1
            species_dict[genus] = tmp_dict
            # species_dict[species] = species_dict.get(species, 0) + 1
        if not fid % 20:
            print(f'{print_timestamp()} {fid+1} reads blasted')

    # Clean up species with few hits
    clean_species_dict = dict()
    for g in species_dict:
        tmp_dict = {s: species_dict[g][s] for s in species_dict[g] if species_dict[g][s] > args.cutoff}
        if sum(tmp_dict.values()) != 0:
            clean_species_dict[g] = tmp_dict
    if not len(clean_species_dict): continue
    species_dict = clean_species_dict
    # species_dict = {k: species_dict[k] for k in species_dict if sum(species_dict[k].values()) > args.cutoff}

    # load in pandas
    idx_tuples = list(chain.from_iterable([[(sd, g) for g in species_dict[sd]] for sd in species_dict]))
    index = pd.MultiIndex.from_tuples(idx_tuples, names=['genus', 'species'])
    species_df = pd.DataFrame(species_dict, index=index, columns=['count'])
    for id in index:
        species_df.loc[id, 'count'] = species_dict[id[0]][id[1]]
    species_df = species_df.sort_values(by='count').sort_values(by='genus')
    species_df.reset_index(inplace=True)

    # Draw barplot
    species_df['cumsum'] = species_df['count'][::-1].cumsum()[::-1]
    col_list = ['#a6611a','#dfc27d','#f5f5f5','#80cdc1']
    col_success = '#018571'
    col_fail = '#ff0000'
    genus_list = []
    plt.clf()
    ax = plt.gca()
    for i, g in enumerate(species_df.groupby('genus', sort=False)):
        edge_idx = False
        genus_list.append(g[0])
        for i2, row in g[1].iterrows():
            sns.set_context(rc={'patch.linewidth': [3, 1][edge_idx]})
            cur_col = col_success if f'{row.genus} {row.species}' in mock_list else col_fail
            sns.barplot(data=row, y='cumsum', color=cur_col, edgecolor='k', ax=ax)
            edge_idx = True
    ax.yaxis.tick_right()
    plt.savefig(f'{outdir_results}barplot_uniqueness_{fq_fn_clean}.png', dpi=400)
    with open(f'{outdir_results}barplot_genuslist_{fq_fn_clean}.txt', 'w') as bpgl:
        for fl in genus_list:
            bpgl.write(f'{fl}\n')

    species_list = [' '.join(idt) for idt in zip(species_df.genus, species_df.species)]
    species_string = '\n'.join(species_list)
    with open(f'{outdir_results}specieslist_{fq_fn_clean}.txt', 'w') as slh:
        slh.write(species_string)
    species_aug_list = [' '.join(idt) for idt in zip(species_list, species_df['count'].astype(str))]
    species_aug_string = '\n'.join(species_aug_list)
    with open(f'{outdir_results}specieslist_counts_{fq_fn_clean}.txt', 'w') as slh:
        slh.write(species_aug_string)
